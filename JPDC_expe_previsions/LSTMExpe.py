import numpy as np
import pandas as pd
import json
from tensorflow.keras.layers import Dense, LSTM
from tensorflow.keras.models import Sequential
from keras.utils import set_random_seed
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
import openTimeSeriesJson
import datetime
import time

set_random_seed(1)

plt.rc('font', size=35)
plt.rcParams['figure.constrained_layout.use'] = True


def filterTime(series, filterTimeInSec):
    seriesFiltered = []
    if filterTimeInSec > 0:
        for value in series[int((filterTimeInSec // 3600) + 1):-int((filterTimeInSec // 3600) + 1)]:
            seriesFiltered.append(value)
    else:
        for value in series:
            seriesFiltered.append(value)
    print("Number of observations =", len(seriesFiltered))
    return seriesFiltered


def createLSTMdict(timeSeries, filter, workloadName, timeSeriesName, steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData, lookback):
    LSTMdict = dict()
    LSTMdict["Workload"] = workloadName
    LSTMdict["TimeSeries"] = timeSeriesName
    LSTMdict["NumberObs"] = len(timeSeries)
    LSTMdict["Filter"] = "[int(("  + str(filter) + "// 3600) + 1):-int((" + str(filter) + "// 3600) + 1)]"
    LSTMdict["StepsPrevision"] = steps
    LSTMdict["NumberPrevisions"] = numberOfPrevisions
    LSTMdict["GapBetweenPrevisionsHours"] = gapBetweenPrevisionsHours
    LSTMdict["LenTrainData"] = lenTrainData
    LSTMdict["Seasons"] = lookback
    return LSTMdict


def computeLSTM(timeSeries, numberOfPrevisions, LSTMdict, lookback):
    MAE = []
    RMSE = []
    Time = []
    REAL = []
    PREDICT = []
    UPPER = []
    LOWER = []

    units = 100
    epochs = 200

    print("lookback =", lookback)
    print("units =", units)
    print("epochs =", epochs)

    # download the data

    base = datetime.datetime(year=2009, month=1, day=1, hour=0)
    hours = pd.date_range(base, base + datetime.timedelta(hours=1999), freq='H')
    series = pd.DataFrame({"Date": hours, "Close": timeSeries[:2000]})
    series.set_index("Date")
    y = series["Close"].values.reshape(-1, 1)

    # scale the data

    scaler = MinMaxScaler(feature_range=(0, 1))
    scaler = scaler.fit(y)
    y = scaler.transform(y)

    # generate the training sequences

    X = []
    Y = []

    for valu in range(lookback, len(y) - steps + 1):
        X.append(y[valu - lookback: valu])
        Y.append(y[valu: valu + steps])

    X = np.array(X)
    Y = np.array(Y)

    # train the model

    start = time.time()

    model = Sequential()
    model.add(LSTM(units=units, input_shape=(lookback, 1)))
    model.add(Dense(steps))

    model.compile(loss='mean_squared_error', optimizer='adam')
    model.fit(X, Y, epochs=epochs, batch_size=32, verbose=0)

    end = time.time()
    dtModel = end - start

    # generate the multi-step forecasts

    for day in range(numberOfPrevisions):
        print("\nDay =", day)

        base = datetime.datetime(year=2009, month=1, day=1, hour=0)
        hours = pd.date_range(base, base + datetime.timedelta(hours=2000 + steps - 1), freq='H')
        series = pd.DataFrame({"Date": hours, "Close": timeSeries[day * 24:2000 + steps + day * 24]})
        series.set_index("Date")
        y = series["Close"].values.reshape(-1, 1)

        # scale the data

        scaler = MinMaxScaler(feature_range=(0, 1))
        scaler = scaler.fit(y)
        y = scaler.transform(y)

        t0 = time.time()

        X_ = y[- lookback - steps: - steps]  # last available input sequence
        X_ = X_.reshape(1, lookback, 1)

        # transform the forecasts back to the original scale

        Y_ = model.predict(X_).reshape(-1, 1)
        Y_ = scaler.inverse_transform(Y_)

        t1 = time.time()
        dt = t1 - t0

        y_to_predict = [int(val) for val in series["Close"][- steps:]]
        y_forecasted = [int(val[0]) for val in Y_]

        REAL.append(y_to_predict)
        PREDICT.append(y_forecasted)

        y_to_predict = np.asarray(y_to_predict)
        y_forecasted = np.asarray(y_forecasted)

        mae = float(np.mean(np.abs(y_to_predict - y_forecasted)))
        rmse = float(np.sqrt(np.mean((y_to_predict - y_forecasted) ** 2)))
        print('MAE', mae)
        print('RMSE', rmse)

        MAE.append(mae)
        RMSE.append(rmse)
        Time.append(dtModel + dt)
        UPPER.append([])
        LOWER.append([])

    LSTMdict["MAE"] = MAE
    LSTMdict["RMSE"] = RMSE
    LSTMdict["Time"] = Time
    LSTMdict["Real"] = REAL
    LSTMdict["Predict"] = PREDICT
    LSTMdict["Upper"] = UPPER
    LSTMdict["Lower"] = LOWER

    return LSTMdict


def jsonResultsLSTM(fileName, LSTMdict):
    with open(fileName, 'w', encoding='utf-8') as f:
        json.dump(LSTMdict, f, indent=4)


def execution(List, steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData):
    for l in List:
        print(l[2])
        print(l[3])
        lookback = l[4]
        X = filterTime(l[0], l[1])
        LSTMdict = createLSTMdict(X, l[1], l[2], l[3], steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData, lookback)
        LSTMdict = computeLSTM(X, numberOfPrevisions, LSTMdict, lookback)
        fileName = "lstmResults_" + str(numberOfPrevisions) + "_Days_" + str(l[2]) + "_" + str(l[3])
        jsonResultsLSTM(fileName, LSTMdict)
        print("\n")


steps = 72
numberOfPrevisions = 40
gapBetweenPrevisionsHours = 24
lenTrainData = 2000

anlIntrepidFilterTime = 2310788
ciematEulerFilterTime = 10890693
metacentrumFilterTime = 18163288
metacentrumFilteredFilterTime = 18163288
pikiplexFilterTime = 5441647
riccFilterTime = 4451683
uniLuGaiaFilterTime = 1800014

executionList = [
    [openTimeSeriesJson.AllocatedCoresSystemAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "AnlIntrepid", "AllocatedCoresSystem", 168],
    [openTimeSeriesJson.AllocatedCoresSystemCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "CiematEuler", "AllocatedCoresSystem", 168],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum", "AllocatedCoresSystem", 168],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "AllocatedCoresSystem", 168],
    [openTimeSeriesJson.AllocatedCoresSystemPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex", "AllocatedCoresSystem", 168],
    # [openTimeSeriesJson.AllocatedCoresSystemRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc", "AllocatedCoresSystem", []],
    # [openTimeSeriesJson.AllocatedCoresSystemUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLuGaia", "AllocatedCoresSystem", [168]],
    [openTimeSeriesJson.AllocatedCoresAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "AnlIntrepid", "AllocatedCoresUsers", 168],
    [openTimeSeriesJson.AllocatedCoresCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "CiematEuler", "AllocatedCoresUsers", 24],
    [openTimeSeriesJson.AllocatedCoresMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum", "AllocatedCoresUsers", 168],
    [openTimeSeriesJson.AllocatedCoresMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "AllocatedCoresUsers", 168],
    [openTimeSeriesJson.AllocatedCoresPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex", "AllocatedCoresUsers", 168],
    # [openTimeSeriesJson.AllocatedCoresRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc", "AllocatedCoresUsers", [24, 168]],
    # [openTimeSeriesJson.AllocatedCoresUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLuGaia", "AllocatedCoresUsers", [24, 168]],
    [openTimeSeriesJson.MassJobsAnlIntrepid["massJobsPerHour"], anlIntrepidFilterTime, "AnlIntrepid", "WorkloadMass", 168],
    [openTimeSeriesJson.MassJobsCiematEuler["massJobsPerHour"], ciematEulerFilterTime, "CiematEuler", "WorkloadMass", 24],
    [openTimeSeriesJson.MassJobsMetacentrum["massJobsPerHour"], metacentrumFilterTime, "Metacentrum", "WorkloadMass", 24],
    [openTimeSeriesJson.MassJobsMetacentrumFiltered["massJobsPerHour"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "WorkloadMass", 24],
    [openTimeSeriesJson.MassJobsPIKIPLEX["massJobsPerHour"], pikiplexFilterTime, "Pikiplex", "WorkloadMass", 24],
    # [openTimeSeriesJson.MassJobsRICC["massJobsPerHour"], riccFilterTime, "Ricc", "WorkloadMass", [24]],
    # [openTimeSeriesJson.MassJobsUniLuGaia["massJobsPerHour"], uniLuGaiaFilterTime, "UniLuGaia", "WorkloadMass", [24]],
    [openTimeSeriesJson.NumberJobsAnlIntrepid["numberJobsPerHour"], anlIntrepidFilterTime, "AnlIntrepid", "NumberOfJobs", 168],
    [openTimeSeriesJson.NumberJobsCiematEuler["numberJobsPerHour"], ciematEulerFilterTime, "CiematEuler", "NumberOfJobs", 24],
    [openTimeSeriesJson.NumberJobsMetacentrum["numberJobsPerHour"], metacentrumFilterTime, "Metacentrum", "NumberOfJobs", 24],
    [openTimeSeriesJson.NumberJobsMetacentrumFiltered["numberJobsPerHour"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "NumberOfJobs", 24],
    [openTimeSeriesJson.NumberJobsPIKIPLEX["numberJobsPerHour"], pikiplexFilterTime, "Pikiplex", "NumberOfJobs", 24]
    # [openTimeSeriesJson.NumberJobsRICC["numberJobsPerHour"], riccFilterTime, "Ricc", "NumberOfJobs", [24, 168]],
    # [openTimeSeriesJson.NumberJobsUniLuGaia["numberJobsPerHour"], uniLuGaiaFilterTime, "UniLuGaia", "NumberOfJobs", [24]]
]

if __name__ == '__main__':
    execution(executionList, steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData)
