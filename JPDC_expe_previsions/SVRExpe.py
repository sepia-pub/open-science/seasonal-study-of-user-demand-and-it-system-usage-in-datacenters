import json
from keras.utils import set_random_seed
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.svm import SVR
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_absolute_error, mean_squared_error
import datetime
import time
import openTimeSeriesJson

set_random_seed(1)

plt.rc('font', size=35)
plt.rcParams['figure.constrained_layout.use'] = True

def create_dataset(X, y, time_steps=1):
    Xs, ys = [], []
    for i in range(len(X) - time_steps):
        v = X[i:i + time_steps]
        Xs.append(v)
        ys.append(y[i + time_steps])
    return np.array(Xs), np.array(ys)

def filterTime(series, filterTimeInSec):
    seriesFiltered = []
    if filterTimeInSec > 0:
        for value in series[int((filterTimeInSec // 3600) + 1):-int((filterTimeInSec // 3600) + 1)]:
            seriesFiltered.append(value)
    else:
        for value in series:
            seriesFiltered.append(value)
    print("Number of observations =", len(seriesFiltered))
    return seriesFiltered


def createSVRdict(timeSeries, filter, workloadName, timeSeriesName, steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData):
    SVRdict = dict()
    SVRdict["Workload"] = workloadName
    SVRdict["TimeSeries"] = timeSeriesName
    SVRdict["NumberObs"] = len(timeSeries)
    SVRdict["Filter"] = "[int(("  + str(filter) + "// 3600) + 1):-int((" + str(filter) + "// 3600) + 1)]"
    SVRdict["StepsPrevision"] = steps
    SVRdict["NumberPrevisions"] = numberOfPrevisions
    SVRdict["GapBetweenPrevisionsHours"] = gapBetweenPrevisionsHours
    SVRdict["LenTrainData"] = lenTrainData
    return SVRdict


def computeSVR(timeSeries, numberOfPrevisions, SVRdict):
    MAE = []
    RMSE = []
    Time = []
    REAL = []
    PREDICT = []
    UPPER = []
    LOWER = []
    PARAM = []

    for day in range(numberOfPrevisions):
        print("\nDay =", day)

        # Load the dataset
        base = datetime.datetime(year=2009, month=1, day=1, hour=0)
        hours = pd.date_range(base, base + datetime.timedelta(hours=1999), freq='H')
        df = pd.DataFrame({"Date": hours, "Close": timeSeries[day * 24:2000 + day * 24]})
        df.set_index("Date", inplace=True)

        # Create training and testing datasets
        train_start_dt = '2009-01-01 00:00:00'
        test_start_dt = '2009-03-16 20:00:00'
        train = df.loc[train_start_dt:test_start_dt]
        test = df.loc[test_start_dt:]

        # Scale the data
        scaler = MinMaxScaler()
        train_scaled = scaler.fit_transform(train)
        test_scaled = scaler.transform(test)

        TIME_STEPS = 168
        X_train, y_train = create_dataset(train_scaled, train_scaled, TIME_STEPS)
        X_test, y_test = create_dataset(test_scaled, test_scaled, TIME_STEPS)

        t0 = time.time()

        rmseRef = 10e10
        paramToKeep = []

        # SVR model
        for C in [0.01, 0.1, 1, 10, 100]:
            for epsilon in [0.001, 0.01, 0.1, 1, 10]:
                for gamma in [0.01, 0.1, 1, 10]:
                    model = SVR(kernel='rbf', gamma=gamma, C=C, epsilon=epsilon)

                    # Fit the model
                    model.fit(X_train.reshape(X_train.shape[0], -1), y_train)

                    # Make predictions
                    train_pred = model.predict(X_train.reshape(X_train.shape[0], -1))
                    test_pred = model.predict(X_test.reshape(X_test.shape[0], -1))

                    # Inverse scaling
                    train_pred_inv = scaler.inverse_transform(train_pred.reshape(-1, 1))
                    y_train_inv = scaler.inverse_transform(y_train.reshape(-1, 1))
                    test_pred_inv = scaler.inverse_transform(test_pred.reshape(-1, 1))
                    y_test_inv = scaler.inverse_transform(y_test.reshape(-1, 1))

                    # Last `TIME_STEPS` values from the test set to start forecasting
                    last_sequence = X_test[-1]

                    # Forecast future values
                    future_forecast = []
                    for _ in range(steps):
                        # Predict next value based on the last sequence
                        next_pred = model.predict(last_sequence.reshape(1, -1))
                        future_forecast.append(next_pred[0])
                        # Update the last sequence by removing the first element and adding the predicted value
                        last_sequence = np.roll(last_sequence, -1)
                        last_sequence[-1] = next_pred

                    # Inverse scaling for future forecast
                    future_forecast_inv = scaler.inverse_transform(np.array(future_forecast).reshape(-1, 1))

                    t1 = time.time()
                    dt = t1 - t0

                    # Generate future timestamps
                    last_date = df.index[-1]
                    future_dates = pd.date_range(start=last_date, periods=steps + 1, freq='H')[1:]

                    base = datetime.datetime(year=2009, month=1, day=1, hour=0)
                    hours = pd.date_range(base, base + datetime.timedelta(hours=steps - 1), freq='H')
                    df_real = pd.DataFrame({"Date": hours, "Close": timeSeries[2000 + day * 24:2000 + day * 24 + steps]})
                    df_real.set_index("Date", inplace=True)

                    y_to_predict = [int(val) for val in df_real["Close"][:]]
                    y_forecasted = [int(val[0]) if val[0] >= 0 else 0 for val in future_forecast_inv]

                    y_to_predict = np.asarray(y_to_predict)
                    y_forecasted = np.asarray(y_forecasted)
                    rmse = float(np.sqrt(np.mean((y_to_predict - y_forecasted) ** 2)))

                    if rmse < rmseRef:
                        rmseRef = rmse
                        paramToKeep = [gamma, C, epsilon]

        model = SVR(kernel='rbf', gamma=paramToKeep[0], C=paramToKeep[1], epsilon=paramToKeep[2])

        # Fit the model
        model.fit(X_train.reshape(X_train.shape[0], -1), y_train)

        # Make predictions
        train_pred = model.predict(X_train.reshape(X_train.shape[0], -1))
        test_pred = model.predict(X_test.reshape(X_test.shape[0], -1))

        # Inverse scaling
        train_pred_inv = scaler.inverse_transform(train_pred.reshape(-1, 1))
        y_train_inv = scaler.inverse_transform(y_train.reshape(-1, 1))
        test_pred_inv = scaler.inverse_transform(test_pred.reshape(-1, 1))
        y_test_inv = scaler.inverse_transform(y_test.reshape(-1, 1))

        # Last `TIME_STEPS` values from the test set to start forecasting
        last_sequence = X_test[-1]

        # Forecast future values
        future_forecast = []
        for _ in range(steps):
            # Predict next value based on the last sequence
            next_pred = model.predict(last_sequence.reshape(1, -1))
            future_forecast.append(next_pred[0])
            # Update the last sequence by removing the first element and adding the predicted value
            last_sequence = np.roll(last_sequence, -1)
            last_sequence[-1] = next_pred

        # Inverse scaling for future forecast
        future_forecast_inv = scaler.inverse_transform(np.array(future_forecast).reshape(-1, 1))

        t1 = time.time()
        dt = t1 - t0

        # Generate future timestamps
        last_date = df.index[-1]
        future_dates = pd.date_range(start=last_date, periods=steps + 1, freq='H')[1:]

        base = datetime.datetime(year=2009, month=1, day=1, hour=0)
        hours = pd.date_range(base, base + datetime.timedelta(hours=steps - 1), freq='H')
        df_real = pd.DataFrame({"Date": hours, "Close": timeSeries[2000 + day * 24:2000 + day * 24 + steps]})
        df_real.set_index("Date", inplace=True)

        y_to_predict = [int(val) for val in df_real["Close"][:]]
        y_forecasted = [int(val[0]) if val[0] >= 0 else 0 for val in future_forecast_inv]

        REAL.append(y_to_predict)
        PREDICT.append(y_forecasted)

        y_to_predict = np.asarray(y_to_predict)
        y_forecasted = np.asarray(y_forecasted)

        mae = float(np.mean(np.abs(y_to_predict - y_forecasted)))
        rmse = float(np.sqrt(np.mean((y_to_predict - y_forecasted) ** 2)))
        print('MAE', mae)
        print('RMSE', rmse)

        MAE.append(mae)
        RMSE.append(rmse)
        Time.append(dt)
        PARAM.append(paramToKeep)
        UPPER.append([])
        LOWER.append([])

    SVRdict["MAE"] = MAE
    SVRdict["RMSE"] = RMSE
    SVRdict["Time"] = Time
    SVRdict["Real"] = REAL
    SVRdict["Predict"] = PREDICT
    SVRdict["Upper"] = UPPER
    SVRdict["Lower"] = LOWER
    SVRdict["Param"] = PARAM

    return SVRdict


def jsonResultsSVR(fileName, SVRdict):
    with open(fileName, 'w', encoding='utf-8') as f:
        json.dump(SVRdict, f, indent=4)


def execution(List, steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData):
    for l in List:
        print(l[2])
        print(l[3])
        X = filterTime(l[0], l[1])
        LSTMdict = createSVRdict(X, l[1], l[2], l[3], steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData)
        LSTMdict = computeSVR(X, numberOfPrevisions, LSTMdict)
        fileName = "svrResults_" + str(numberOfPrevisions) + "_Days_" + str(l[2]) + "_" + str(l[3])
        jsonResultsSVR(fileName, LSTMdict)
        print("\n")


steps = 72
numberOfPrevisions = 40
gapBetweenPrevisionsHours = 24
lenTrainData = 2000

anlIntrepidFilterTime = 2310788
ciematEulerFilterTime = 10890693
metacentrumFilterTime = 18163288
metacentrumFilteredFilterTime = 18163288
pikiplexFilterTime = 5441647
riccFilterTime = 4451683
uniLuGaiaFilterTime = 1800014

executionList = [
    [openTimeSeriesJson.AllocatedCoresSystemAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "AnlIntrepid", "AllocatedCoresSystem"],
    [openTimeSeriesJson.AllocatedCoresSystemCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "CiematEuler", "AllocatedCoresSystem"],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum", "AllocatedCoresSystem"],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "AllocatedCoresSystem"],
    [openTimeSeriesJson.AllocatedCoresSystemPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex", "AllocatedCoresSystem"],
    # [openTimeSeriesJson.AllocatedCoresSystemRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc", "AllocatedCoresSystem"],
    # [openTimeSeriesJson.AllocatedCoresSystemUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLuGaia", "AllocatedCoresSystem"],
    [openTimeSeriesJson.AllocatedCoresAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "AnlIntrepid", "AllocatedCoresUsers"],
    [openTimeSeriesJson.AllocatedCoresCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "CiematEuler", "AllocatedCoresUsers"],
    [openTimeSeriesJson.AllocatedCoresMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum", "AllocatedCoresUsers"],
    [openTimeSeriesJson.AllocatedCoresMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "AllocatedCoresUsers"],
    [openTimeSeriesJson.AllocatedCoresPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex", "AllocatedCoresUsers"],
    # [openTimeSeriesJson.AllocatedCoresRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc", "AllocatedCoresUsers"],
    # [openTimeSeriesJson.AllocatedCoresUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLuGaia", "AllocatedCoresUsers"],
    [openTimeSeriesJson.MassJobsAnlIntrepid["massJobsPerHour"], anlIntrepidFilterTime, "AnlIntrepid", "WorkloadMass"],
    [openTimeSeriesJson.MassJobsCiematEuler["massJobsPerHour"], ciematEulerFilterTime, "CiematEuler", "WorkloadMass"],
    [openTimeSeriesJson.MassJobsMetacentrum["massJobsPerHour"], metacentrumFilterTime, "Metacentrum", "WorkloadMass"],
    [openTimeSeriesJson.MassJobsMetacentrumFiltered["massJobsPerHour"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "WorkloadMass"],
    [openTimeSeriesJson.MassJobsPIKIPLEX["massJobsPerHour"], pikiplexFilterTime, "Pikiplex", "WorkloadMass"],
    # [openTimeSeriesJson.MassJobsRICC["massJobsPerHour"], riccFilterTime, "Ricc", "WorkloadMass"],
    # [openTimeSeriesJson.MassJobsUniLuGaia["massJobsPerHour"], uniLuGaiaFilterTime, "UniLuGaia", "WorkloadMass"],
    [openTimeSeriesJson.NumberJobsAnlIntrepid["numberJobsPerHour"], anlIntrepidFilterTime, "AnlIntrepid", "NumberOfJobs"],
    [openTimeSeriesJson.NumberJobsCiematEuler["numberJobsPerHour"], ciematEulerFilterTime, "CiematEuler", "NumberOfJobs"],
    [openTimeSeriesJson.NumberJobsMetacentrum["numberJobsPerHour"], metacentrumFilterTime, "Metacentrum", "NumberOfJobs"],
    [openTimeSeriesJson.NumberJobsMetacentrumFiltered["numberJobsPerHour"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "NumberOfJobs"],
    [openTimeSeriesJson.NumberJobsPIKIPLEX["numberJobsPerHour"], pikiplexFilterTime, "Pikiplex", "NumberOfJobs"]
    # [openTimeSeriesJson.NumberJobsRICC["numberJobsPerHour"], riccFilterTime, "Ricc", "NumberOfJobs"],
    # [openTimeSeriesJson.NumberJobsUniLuGaia["numberJobsPerHour"], uniLuGaiaFilterTime, "UniLuGaia", "NumberOfJobs"]
]

if __name__ == '__main__':
    execution(executionList, steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData)