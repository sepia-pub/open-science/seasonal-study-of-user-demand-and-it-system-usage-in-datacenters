import json
import matplotlib.pyplot as plt
import numpy as np

plt.rc('font', size=35)
plt.rcParams['figure.constrained_layout.use'] = True

def loadData(filenameList):
    nom_fichier = "/home/dlandre/Doctorat/phd1/Workload_Prevision/previsionExperiments/BiLSTMResultsJson/biLstmResults_40_Days_" + filenameList + ".json"
    with open(nom_fichier, 'r') as file:
        BiLSTM = json.load(file)

    nom_fichier = "/home/dlandre/Doctorat/phd1/Workload_Prevision/previsionExperiments/LSTMResultsJson/lstmResults_40_Days_" + filenameList + ".json"
    with open(nom_fichier, 'r') as file:
        LSTM = json.load(file)

    nom_fichier = "/home/dlandre/Doctorat/phd1/Workload_Prevision/previsionExperiments/ProphetResultsJson/prophetResults_40_Days_" + filenameList + ".json"
    with open(nom_fichier, 'r') as file:
        Prophet = json.load(file)

    nom_fichier = "/home/dlandre/Doctorat/phd1/Workload_Prevision/previsionExperiments/SVRResultsJson/svrResults_40_Days_" + filenameList + ".json"
    with open(nom_fichier, 'r') as file:
        SVR = json.load(file)

    nom_fichier = "/home/dlandre/Doctorat/phd1/Workload_Prevision/previsionExperiments/TbatsResultsJson/tbatsResults_40_Days_" + filenameList + ".json"
    with open(nom_fichier, 'r') as file:
        TBATS = json.load(file)

    return BiLSTM, LSTM, Prophet, SVR, TBATS

# Interval

def computePercentageOfPrevisionInInterval(dataDict):
    PercentageList = []
    N = 0
    D = 0
    for upper, lower, real in zip(dataDict["Upper"], dataDict["Lower"], dataDict["Real"]):
        s = 0
        for i in range(len(real)):
            valueUpper = 0 if upper[i] < 0 else upper[i]
            valueLower = 0 if lower[i] < 0 else lower[i]
            valueReal = 0 if real[i] < 0 else real[i]
            if valueLower <= valueReal <= valueUpper:
                s += 1
        N += s
        D += len(real)
        PercentageList.append(round(s/len(real)*100, 1))
    PercentageTot = N / D * 100
    return PercentageList, PercentageTot

# MAE

def computeMAE(dataDict):
    MAE = []
    for predict, real in zip(dataDict["Predict"], dataDict["Real"]):
        s = 0
        for i in range(len(predict)):
            valuePredict = 0 if predict[i] < 0 else predict[i]
            valueActual = 0 if real[i] < 0 else real[i]
            s += np.abs(valuePredict - valueActual)
        s = s / len(predict)
        MAE.append(s)
    return MAE

def computeAllMAE(BiLSTM, LSTM, Prophet, SVR, TBATS):
    BiLstmMAE = computeMAE(BiLSTM)
    LstmMAE = computeMAE(LSTM)
    ProphetMAE = computeMAE(Prophet)
    SvrMAE = computeMAE(SVR)
    TbatsMAE = computeMAE(TBATS)
    return BiLstmMAE, LstmMAE, ProphetMAE, SvrMAE, TbatsMAE

def plotMAE(BiLSTM, LSTM, Prophet, SVR, TBATS):
    plt.plot(BiLSTM, label="BiLSTM", color="black")
    plt.plot(LSTM, label="LSTM", color="blue")
    plt.plot(Prophet, label="Prophet", color="red")
    plt.plot(SVR, label="SVR", color="green")
    plt.plot(TBATS, label="TBATS", color="orange")
    plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
    plt.xlabel('Time (day)', fontsize=45)
    plt.ylabel('MAE', fontsize=45)
    plt.xticks(fontsize=40)
    plt.yticks(fontsize=40)
    plt.legend()
    plt.show()

def plotMAEBox(BiLSTM, LSTM, Prophet, SVR, TBATS):
    # fig, ax = plt.subplots()
    # MaeBox = dict()
    # MaeBox['BiLSTM'] = [item for item in BiLSTM]
    # MaeBox['LSTM'] = [item for item in LSTM]
    # MaeBox['Prophet'] = [item for item in Prophet]
    # MaeBox['SVR'] = [item for item in SVR]
    # MaeBox['TBATS'] = [item for item in TBATS]
    #
    # bp = ax.boxplot(MaeBox.values(), showmeans=True, sym='+',
    #                 meanprops=dict(markeredgecolor='black', markerfacecolor='firebrick'),
    #                 medianprops=dict(color='firebrick'))
    # col = ['black', 'black', 'black', 'black', 'black']
    # for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
    #     i = 0
    #     j = 0
    #     for box in bp[element]:
    #         box.set(color=col[i], linewidth=5)
    #         if element == 'whiskers' or element == 'caps':
    #             if j == 0:
    #                 j += 1
    #             else:
    #                 i += 1
    #                 j = 0
    #         else:
    #             i += 1
    #
    # ax.set_xticklabels(MaeBox.keys())
    # ax.yaxis.grid(True, linestyle='-', which='major', alpha=0.5)
    # ax.set_ylabel('MAE', fontsize=45)
    # plt.xticks(fontsize=40)
    # plt.yticks(fontsize=40)
    # plt.show()

    print("BiLSTM MAE mean  =", round(np.mean(BiLSTM), 1), "| std =", round(np.std(BiLSTM), 1))
    print("LSTM MAE mean    =", round(np.mean(LSTM), 1), "| std =", round(np.std(LSTM), 1))
    print("Prophet MAE mean =", round(np.mean(Prophet), 1), "| std =", round(np.std(Prophet), 1))
    print("SVR MAE mean     =", round(np.mean(SVR), 1), "| std =", round(np.std(SVR), 1))
    print("TBATS MAE mean   =", round(np.mean(TBATS), 1), "| std =", round(np.std(TBATS), 1))

# RMSE

def computeRMSE(dataDict):
    RMSE = []
    for predict, real in zip(dataDict["Predict"], dataDict["Real"]):
        s = 0
        for i in range(len(predict)):
            valuePredict = 0 if predict[i] < 0 else predict[i]
            valueActual = 0 if real[i] < 0 else real[i]
            s += (valuePredict - valueActual)**2
        s = np.sqrt(s / len(predict))
        RMSE.append(s)
    return RMSE

def computeAllRMSE(BiLSTM, LSTM, Prophet, SVR, TBATS):
    BiLstmRMSE = computeRMSE(BiLSTM)
    LstmRMSE = computeRMSE(LSTM)
    ProphetRMSE = computeRMSE(Prophet)
    SvrRMSE = computeRMSE(SVR)
    TbatsRMSE = computeRMSE(TBATS)
    return BiLstmRMSE, LstmRMSE, ProphetRMSE, SvrRMSE, TbatsRMSE

def plotRMSE(BiLSTM, LSTM, Prophet, SVR, TBATS):
    plt.plot(BiLSTM, label="BiLSTM", color="black")
    plt.plot(LSTM, label="LSTM", color="blue")
    plt.plot(Prophet, label="Prophet", color="red")
    plt.plot(SVR, label="SVR", color="green")
    plt.plot(TBATS, label="TBATS", color="orange")
    plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
    plt.xlabel('Time (day)', fontsize=45)
    plt.ylabel('RMSE', fontsize=45)
    plt.xticks(fontsize=40)
    plt.yticks(fontsize=40)
    plt.legend()
    plt.show()

def plotRMSEBox(BiLSTM, LSTM, Prophet, SVR, TBATS):
    # fig, ax = plt.subplots()
    # RmseBox = dict()
    # RmseBox['BiLSTM'] = [item for item in BiLSTM]
    # RmseBox['LSTM'] = [item for item in LSTM]
    # RmseBox['Prophet'] = [item for item in Prophet]
    # RmseBox['SVR'] = [item for item in SVR]
    # RmseBox['TBATS'] = [item for item in TBATS]
    #
    # bp = ax.boxplot(RmseBox.values(), showmeans=True, sym='+',
    #                 meanprops=dict(markeredgecolor='black', markerfacecolor='firebrick'),
    #                 medianprops=dict(color='firebrick'))
    # col = ['black', 'black', 'black', 'black', 'black']
    # for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
    #     i = 0
    #     j = 0
    #     for box in bp[element]:
    #         box.set(color=col[i], linewidth=5)
    #         if element == 'whiskers' or element == 'caps':
    #             if j == 0:
    #                 j += 1
    #             else:
    #                 i += 1
    #                 j = 0
    #         else:
    #             i += 1
    #
    # ax.set_xticklabels(RmseBox.keys())
    # ax.yaxis.grid(True, linestyle='-', which='major', alpha=0.5)
    # ax.set_ylabel('RMSE', fontsize=45)
    # plt.xticks(fontsize=40)
    # plt.yticks(fontsize=40)
    # plt.show()

    print("BiLSTM RMSE mean  =", round(np.mean(BiLSTM), 1), "| std =", round(np.std(BiLSTM), 1))
    print("LSTM RMSE mean    =", round(np.mean(LSTM), 1), "| std =", round(np.std(LSTM), 1))
    print("Prophet RMSE mean =", round(np.mean(Prophet), 1), "| std =", round(np.std(Prophet), 1))
    print("SVR RMSE mean     =", round(np.mean(SVR), 1), "| std =", round(np.std(SVR), 1))
    print("TBATS RMSE mean   =", round(np.mean(TBATS), 1), "| std =", round(np.std(TBATS), 1))

# RMLSE

def computeRMSLE(dataDict):
    RMSLE = []
    for predict, real in zip(dataDict["Predict"], dataDict["Real"]):
        s = 0
        for i in range(len(predict)):
            valuePredict = 0 if predict[i] < 0 else predict[i]
            valueActual = 0 if real[i] < 0 else real[i]
            s += np.log(valuePredict + 1) + np.log(valueActual + 1)
        s = np.sqrt(s / len(predict))
        RMSLE.append(s)
    return RMSLE

def computeAllRMSLE(BiLSTM, LSTM, Prophet, SVR, TBATS):
    BiLstmRMSLE = computeRMSLE(BiLSTM)
    LstmRMSLE = computeRMSLE(LSTM)
    ProphetRMSLE = computeRMSLE(Prophet)
    SvrRMSLE = computeRMSLE(SVR)
    TbatsRMSLE = computeRMSLE(TBATS)
    return BiLstmRMSLE, LstmRMSLE, ProphetRMSLE, SvrRMSLE, TbatsRMSLE

def plotRMSLE(BiLstmRMSLE, LstmRMSLE, ProphetRMSLE, SvrRMSLE, TbatsRMSLE):
    plt.plot(BiLstmRMSLE, label="BiLSTM", color="black")
    plt.plot(LstmRMSLE, label="LSTM", color="blue")
    plt.plot(ProphetRMSLE, label="Prophet", color="red")
    plt.plot(SvrRMSLE, label="SVR", color="green")
    plt.plot(TbatsRMSLE, label="TBATS", color="orange")
    plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
    plt.xlabel('Time (day)', fontsize=45)
    plt.ylabel('RMSLE', fontsize=45)
    plt.xticks(fontsize=40)
    plt.yticks(fontsize=40)
    plt.legend()
    plt.show()

def plotRMSLEBox(BiLstmRMSLE, LstmRMSLE, ProphetRMSLE, SvrRMSLE, TbatsRMSLE):
    # fig, ax = plt.subplots()
    # RmsleBox = dict()
    # RmsleBox['BiLSTM'] = [item for item in BiLstmRMSLE]
    # RmsleBox['LSTM'] = [item for item in LstmRMSLE]
    # RmsleBox['Prophet'] = [item for item in ProphetRMSLE]
    # RmsleBox['SVR'] = [item for item in SvrRMSLE]
    # RmsleBox['TBATS'] = [item for item in TbatsRMSLE]
    #
    # bp = ax.boxplot(RmsleBox.values(), showmeans=True, sym='+',
    #                 meanprops=dict(markeredgecolor='black', markerfacecolor='firebrick'),
    #                 medianprops=dict(color='firebrick'))
    # col = ['black', 'black', 'black', 'black', 'black']
    # for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
    #     i = 0
    #     j = 0
    #     for box in bp[element]:
    #         box.set(color=col[i], linewidth=5)
    #         if element == 'whiskers' or element == 'caps':
    #             if j == 0:
    #                 j += 1
    #             else:
    #                 i += 1
    #                 j = 0
    #         else:
    #             i += 1
    #
    # ax.set_xticklabels(RmsleBox.keys())
    # ax.yaxis.grid(True, linestyle='-', which='major', alpha=0.5)
    # ax.set_ylabel('RMSLE', fontsize=45)
    # plt.xticks(fontsize=40)
    # plt.yticks(fontsize=40)
    # plt.show()

    print("BiLSTM RMSLE mean  =", round(np.mean(BiLstmRMSLE), 1), "| std =", round(np.std(BiLstmRMSLE), 1))
    print("LSTM RMSLE mean    =", round(np.mean(LstmRMSLE), 1), "| std =", round(np.std(LstmRMSLE), 1))
    print("Prophet RMSLE mean =", round(np.mean(ProphetRMSLE), 1), "| std =", round(np.std(ProphetRMSLE), 1))
    print("SVR RMSLE mean     =", round(np.mean(SvrRMSLE), 1), "| std =", round(np.std(SvrRMSLE), 1))
    print("TBATS RMSLE mean   =", round(np.mean(TbatsRMSLE), 1), "| std =", round(np.std(TbatsRMSLE), 1))
# Time

def plotTime(BiLSTM, LSTM, Prophet, SVR, TBATS):
    plt.plot(BiLSTM["Time"], label="BiLSTM", color="black")
    plt.plot(LSTM["Time"], label="LSTM", color="blue")
    plt.plot(Prophet["Time"], label="Prophet", color="red")
    plt.plot(SVR["Time"], label="SVR", color="green")
    plt.plot(TBATS["Time"], label="TBATS", color="orange")
    plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
    plt.xlabel('Time (day)', fontsize=45)
    plt.ylabel('Execution time (s)', fontsize=45)
    plt.xticks(fontsize=40)
    plt.yticks(fontsize=40)
    plt.legend()
    plt.show()

    print("BiLSTM Time mean  =", round(np.mean(BiLSTM["Time"]), 1), "| std =", round(np.std(BiLSTM["Time"]), 1))
    print("LSTM Time mean    =", round(np.mean(LSTM["Time"]), 1), "| std =", round(np.std(LSTM["Time"]), 1))
    print("Prophet Time mean =", round(np.mean(Prophet["Time"]), 1), "| std =", round(np.std(Prophet["Time"]), 1))
    print("SVR Time mean     =", round(np.mean(SVR["Time"]), 1), "| std =", round(np.std(SVR["Time"]), 1))
    print("TBATS Time mean   =", round(np.mean(TBATS["Time"]), 1), "| std =", round(np.std(TBATS["Time"]), 1))

filenameList = ["AnlIntrepid_AllocatedCoresSystem",
                "CiematEuler_AllocatedCoresSystem",
                "Metacentrum_AllocatedCoresSystem",
                "MetacentrumFiltered_AllocatedCoresSystem",
                "Pikiplex_AllocatedCoresSystem",

                "AnlIntrepid_AllocatedCoresUsers",
                "CiematEuler_AllocatedCoresUsers",
                "Metacentrum_AllocatedCoresUsers",
                "MetacentrumFiltered_AllocatedCoresUsers",
                "Pikiplex_AllocatedCoresUsers",

                "AnlIntrepid_NumberOfJobs",
                "CiematEuler_NumberOfJobs",
                "Metacentrum_NumberOfJobs",
                "MetacentrumFiltered_NumberOfJobs",
                "Pikiplex_NumberOfJobs",

                "AnlIntrepid_WorkloadMass",
                "CiematEuler_WorkloadMass",
                "Metacentrum_WorkloadMass",
                "MetacentrumFiltered_WorkloadMass",
                "Pikiplex_WorkloadMass"]

for ind in range(len(filenameList)):
    filename = filenameList[ind]
    print("#######\n" + filename + "\n#######")
    BiLSTM, LSTM, Prophet, SVR, TBATS = loadData(filename)
    BiLstmMAE, LstmMAE, ProphetMAE, SvrMAE, TbatsMAE = computeAllMAE(BiLSTM, LSTM, Prophet, SVR, TBATS)
    print("-> MAE")
    plotMAE(BiLstmMAE, LstmMAE, ProphetMAE, SvrMAE, TbatsMAE)
    plotMAEBox(BiLstmMAE, LstmMAE, ProphetMAE, SvrMAE, TbatsMAE)
    BiLstmRMSE, LstmRMSE, ProphetRMSE, SvrRMSE, TbatsRMSE = computeAllRMSE(BiLSTM, LSTM, Prophet, SVR, TBATS)
    print("-> RMSE")
    plotRMSE(BiLstmRMSE, LstmRMSE, ProphetRMSE, SvrRMSE, TbatsRMSE)
    plotRMSEBox(BiLstmRMSE, LstmRMSE, ProphetRMSE, SvrRMSE, TbatsRMSE)
    BiLstmRMSLE, LstmRMSLE, ProphetRMSLE, SvrRMSLE, TbatsRMSLE = computeAllRMSLE(BiLSTM, LSTM, Prophet, SVR, TBATS)
    print("-> RMSLE")
    plotRMSLE(BiLstmRMSLE, LstmRMSLE, ProphetRMSLE, SvrRMSLE, TbatsRMSLE)
    plotRMSLEBox(BiLstmRMSLE, LstmRMSLE, ProphetRMSLE, SvrRMSLE, TbatsRMSLE)
    print("-> Time")
    plotTime(BiLSTM, LSTM, Prophet, SVR, TBATS)
    print("-> Interval")
    percentageListProphet, percentageTotProphet = computePercentageOfPrevisionInInterval(Prophet)
    print("Prophet : percentageTot =", round(percentageTotProphet, 1), "| PercentageList =", percentageListProphet)
    percentageListTBATS, percentageTotTBATS = computePercentageOfPrevisionInInterval(TBATS)
    print("TBATS : percentageTot =", round(percentageTotTBATS, 1), "| PercentageList =", percentageListTBATS)
    print("\n")