import json

### Maximum number of allocated cores by the system

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresSystemAnlIntrepid.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresSystemAnlIntrepid = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresSystemCiematEuler.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresSystemCiematEuler = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresSystemMetacentrum.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresSystemMetacentrum = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresSystemMetacentrumFiltered.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresSystemMetacentrumFiltered = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresSystemPIKIPLEX.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresSystemPIKIPLEX = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresSystemRICC.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresSystemRICC = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresSystemUniLuGaia.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresSystemUniLuGaia = json.load(file)

### Maximum number of requested cores by the users

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresAnlIntrepid.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresAnlIntrepid = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresCiematEuler.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresCiematEuler = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresMetacentrum.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresMetacentrum = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresMetacentrumFiltered.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresMetacentrumFiltered = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresPIKIPLEX.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresPIKIPLEX = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresRICC.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresRICC = json.load(file)

nom_fichier = "timeSeriesJsonHPC/AllocatedCoresUniLuGaia.json"
with open(nom_fichier, 'r') as file:
    AllocatedCoresUniLuGaia = json.load(file)

### Workload mass

nom_fichier = "timeSeriesJsonHPC/MassJobsAnlIntrepid.json"
with open(nom_fichier, 'r') as file:
    MassJobsAnlIntrepid = json.load(file)

nom_fichier = "timeSeriesJsonHPC/MassJobsCiematEuler.json"
with open(nom_fichier, 'r') as file:
    MassJobsCiematEuler = json.load(file)

nom_fichier = "timeSeriesJsonHPC/MassJobsMetacentrum.json"
with open(nom_fichier, 'r') as file:
    MassJobsMetacentrum = json.load(file)

nom_fichier = "timeSeriesJsonHPC/MassJobsMetacentrumFiltered.json"
with open(nom_fichier, 'r') as file:
    MassJobsMetacentrumFiltered = json.load(file)

nom_fichier = "timeSeriesJsonHPC/MassJobsPIKIPLEX.json"
with open(nom_fichier, 'r') as file:
    MassJobsPIKIPLEX = json.load(file)

nom_fichier = "timeSeriesJsonHPC/MassJobsRICC.json"
with open(nom_fichier, 'r') as file:
    MassJobsRICC = json.load(file)

nom_fichier = "timeSeriesJsonHPC/MassJobsUniLuGaia.json"
with open(nom_fichier, 'r') as file:
    MassJobsUniLuGaia = json.load(file)

### Number of jobs

nom_fichier = "timeSeriesJsonHPC/NumberJobsAnlIntrepid.json"
with open(nom_fichier, 'r') as file:
    NumberJobsAnlIntrepid = json.load(file)

nom_fichier = "timeSeriesJsonHPC/NumberJobsCiematEuler.json"
with open(nom_fichier, 'r') as file:
    NumberJobsCiematEuler = json.load(file)

nom_fichier = "timeSeriesJsonHPC/NumberJobsMetacentrum.json"
with open(nom_fichier, 'r') as file:
    NumberJobsMetacentrum = json.load(file)

nom_fichier = "timeSeriesJsonHPC/NumberJobsMetacentrumFiltered.json"
with open(nom_fichier, 'r') as file:
    NumberJobsMetacentrumFiltered = json.load(file)

nom_fichier = "timeSeriesJsonHPC/NumberJobsPIKIPLEX.json"
with open(nom_fichier, 'r') as file:
    NumberJobsPIKIPLEX = json.load(file)

nom_fichier = "timeSeriesJsonHPC/NumberJobsRICC.json"
with open(nom_fichier, 'r') as file:
    NumberJobsRICC = json.load(file)

nom_fichier = "timeSeriesJsonHPC/NumberJobsUniLuGaia.json"
with open(nom_fichier, 'r') as file:
    NumberJobsUniLuGaia = json.load(file)