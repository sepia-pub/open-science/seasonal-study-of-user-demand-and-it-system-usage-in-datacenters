import pandas as pd
from prophet import Prophet
import datetime
import json
import matplotlib.pyplot as plt
import numpy as np
import time
import openTimeSeriesJson

plt.rc('font', size=35)
plt.rcParams['figure.constrained_layout.use'] = True


def filterTime(series, filterTimeInSec):
    seriesFiltered = []
    if filterTimeInSec > 0:
        for value in series[int((filterTimeInSec // 3600) + 1):-int((filterTimeInSec // 3600) + 1)]:
            seriesFiltered.append(value)
    else:
        for value in series:
            seriesFiltered.append(value)
    print("Number of observations =", len(seriesFiltered))
    return seriesFiltered


def createProphetdict(timeSeries, filter, workloadName, timeSeriesName, steps, numberOfPrevisions,
                      gapBetweenPrevisionsHours, lenTrainData, seasons):
    Prophetdict = dict()
    Prophetdict["Workload"] = workloadName
    Prophetdict["TimeSeries"] = timeSeriesName
    Prophetdict["NumberObs"] = len(timeSeries)
    Prophetdict["Filter"] = "[int((" + str(filter) + "// 3600) + 1):-int((" + str(filter) + "// 3600) + 1)]"
    Prophetdict["StepsPrevision"] = steps
    Prophetdict["NumberPrevisions"] = numberOfPrevisions
    Prophetdict["GapBetweenPrevisionsHours"] = gapBetweenPrevisionsHours
    Prophetdict["LenTrainData"] = lenTrainData
    Prophetdict["Seasons"] = str(seasons)
    return Prophetdict


def computeProphet(timeSeries, filter, seasons, numberOfPrevisions, Prophetdict, startTime, holidays):
    MAE = []
    RMSE = []
    Time = []
    REAL = []
    PREDICT = []
    UPPER = []
    LOWER = []

    for day in range(numberOfPrevisions):
        print("\nDay =", day)

        y_p = np.asarray(timeSeries)[day * 24:2000 + steps + day * 24]

        print("len(y_p) =", len(y_p))

        base = datetime.datetime(year=startTime[0], month=startTime[1], day=startTime[2], hour=startTime[3],
                                 minute=startTime[4], second=startTime[5])  # Workload start time
        hours = [
            base + datetime.timedelta(seconds=filter) + datetime.timedelta(hours=day * 24) + datetime.timedelta(hours=x)
            for x in range(len(y_p))]

        df = pd.DataFrame(list(zip(hours, y_p)), columns=["ds", "y"])

        df['floor'] = 0

        daySeason = False
        semiWeekSeason = False
        weekSeason = False
        monthlySeason = False
        twoMonthsSeason = False

        for value in seasons:
            if value == 24:
                daySeason = True
                continue
            if value == 84:
                semiWeekSeason = True
                continue
            if value == 168:
                weekSeason = True
                continue
            if value == 730:
                monthlySeason = True
                continue
            if value == 1460:
                twoMonthsSeason = True
                continue

        t0 = time.time()

        estimator = Prophet(seasonality_mode='additive',
                            daily_seasonality=daySeason,
                            weekly_seasonality=weekSeason,
                            yearly_seasonality=False)
        estimator.add_country_holidays(country_name=holidays)
        if semiWeekSeason:
            estimator.add_seasonality(name='semi-weekly', period=3.5, fourier_order=2)
        if monthlySeason:
            estimator.add_seasonality(name='monthly', period=30.5, fourier_order=5)
        if twoMonthsSeason:
            estimator.add_seasonality(name='two months', period=61, fourier_order=6)

        estimator.fit(df[:-steps])
        future = estimator.make_future_dataframe(periods=steps, freq='H')
        future['floor'] = 0
        forecast = estimator.predict(future)

        t1 = time.time()
        dt = t1 - t0
        print('dt =', dt)

        y_to_predict = y_p[(len(y_p) - steps):]
        y_for = forecast['yhat'].tolist()[len(y_p) - steps:]
        yhat_lower = forecast['yhat_lower'].tolist()[len(y_p) - steps:]
        yhat_upper = forecast['yhat_upper'].tolist()[len(y_p) - steps:]

        yhat_upper_adjusted = []
        for val in yhat_upper:
            yhat_upper_adjusted.append(int(val))

        yhat_lower_adjusted = []
        for val in yhat_lower:
            if val < 0:
                yhat_lower_adjusted.append(0)
            else:
                yhat_lower_adjusted.append(int(val))

        y_forecasted = []
        for val in y_for:
            y_forecasted.append(int(val))

        mae = float(np.mean(np.abs(y_to_predict - y_forecasted)))
        rmse = float(np.sqrt(np.mean((y_to_predict - y_forecasted) ** 2)))
        print('MAE', mae)
        print('RMSE', rmse)

        y_to_predict_adjusted = []
        for val in y_to_predict:
            y_to_predict_adjusted.append(int(val))

        MAE.append(mae)
        RMSE.append(rmse)
        Time.append(dt)
        REAL.append(y_to_predict_adjusted)
        PREDICT.append(y_forecasted)
        UPPER.append(yhat_upper_adjusted)
        LOWER.append(yhat_lower_adjusted)

    Prophetdict["MAE"] = MAE
    Prophetdict["RMSE"] = RMSE
    Prophetdict["Time"] = Time
    Prophetdict["Real"] = REAL
    Prophetdict["Predict"] = PREDICT
    Prophetdict["Upper"] = UPPER
    Prophetdict["Lower"] = LOWER

    return Prophetdict


def jsonResultsProphet(fileName, Prophetdict):
    with open(fileName, 'w', encoding='utf-8') as f:
        json.dump(Prophetdict, f, indent=4)


def execution(List, steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData):
    for l in List:
        print(l[2])
        print(l[3])
        seasons = str(l[4])
        X = filterTime(l[0], l[1])
        Prophetdict = createProphetdict(X, l[1], l[2], l[3], steps, numberOfPrevisions, gapBetweenPrevisionsHours,
                                        lenTrainData, seasons)
        Prophetdict = computeProphet(X, l[1], l[4], numberOfPrevisions, Prophetdict, l[5], l[6])
        fileName = "prophetResults_" + str(numberOfPrevisions) + "_Days_" + str(l[2]) + "_" + str(l[3])
        jsonResultsProphet(fileName, Prophetdict)
        print("\n")


steps = 72
numberOfPrevisions = 40
gapBetweenPrevisionsHours = 24
lenTrainData = 2000

anlIntrepidFilterTime = 2310788
ciematEulerFilterTime = 10890693
metacentrumFilterTime = 18163288
metacentrumFilteredFilterTime = 18163288
pikiplexFilterTime = 5441647
riccFilterTime = 4451683
uniLuGaiaFilterTime = 1800014

executionList = [
    [openTimeSeriesJson.AllocatedCoresSystemAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "AnlIntrepid",
     "AllocatedCoresSystem", [24, 84, 168], [2009, 1, 5, 0, 0, 24], 'US'],
    [openTimeSeriesJson.AllocatedCoresSystemCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "CiematEuler",
     "AllocatedCoresSystem", [24, 168], [2008, 11, 17, 14, 1, 47], 'ES'],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum",
     "AllocatedCoresSystem", [24, 168, 730], [2013, 1, 1, 0, 0, 6], 'CZ'],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime,
     "MetacentrumFiltered", "AllocatedCoresSystem", [168, 730], [2013, 1, 1, 0, 0, 6], 'CZ'],
    [openTimeSeriesJson.AllocatedCoresSystemPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex",
     "AllocatedCoresSystem", [24, 168, 1460], [2009, 4, 9, 11, 5, 22], 'DE'],
    # [openTimeSeriesJson.AllocatedCoresSystemRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc", "AllocatedCoresSystem", []],
    # [openTimeSeriesJson.AllocatedCoresSystemUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLuGaia", "AllocatedCoresSystem", [168]],
    [openTimeSeriesJson.AllocatedCoresAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "AnlIntrepid",
     "AllocatedCoresUsers", [24, 168], [2009, 1, 5, 0, 0, 24], 'US'],
    [openTimeSeriesJson.AllocatedCoresCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "CiematEuler",
     "AllocatedCoresUsers", [24], [2008, 11, 17, 14, 1, 47], 'ES'],
    [openTimeSeriesJson.AllocatedCoresMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum",
     "AllocatedCoresUsers", [24, 168, 730], [2013, 1, 1, 0, 0, 6], 'CZ'],
    [openTimeSeriesJson.AllocatedCoresMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime,
     "MetacentrumFiltered", "AllocatedCoresUsers", [24, 168, 730], [2013, 1, 1, 0, 0, 6], 'CZ'],
    [openTimeSeriesJson.AllocatedCoresPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex",
     "AllocatedCoresUsers", [24, 168, 1460], [2009, 4, 9, 11, 5, 22], 'DE'],
    # [openTimeSeriesJson.AllocatedCoresRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc", "AllocatedCoresUsers", [24, 168]],
    # [openTimeSeriesJson.AllocatedCoresUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLuGaia", "AllocatedCoresUsers", [24, 168]],
    [openTimeSeriesJson.MassJobsAnlIntrepid["massJobsPerHour"], anlIntrepidFilterTime, "AnlIntrepid", "WorkloadMass",
     [24, 168], [2009, 1, 5, 0, 0, 24], 'US'],
    [openTimeSeriesJson.MassJobsCiematEuler["massJobsPerHour"], ciematEulerFilterTime, "CiematEuler", "WorkloadMass",
     [24], [2008, 11, 17, 14, 1, 47], 'ES'],
    [openTimeSeriesJson.MassJobsMetacentrum["massJobsPerHour"], metacentrumFilterTime, "Metacentrum", "WorkloadMass",
     [24], [2013, 1, 1, 0, 0, 6], 'CZ'],
    [openTimeSeriesJson.MassJobsMetacentrumFiltered["massJobsPerHour"], metacentrumFilteredFilterTime,
     "MetacentrumFiltered", "WorkloadMass", [], [2013, 1, 1, 0, 0, 6], 'CZ'],
    [openTimeSeriesJson.MassJobsPIKIPLEX["massJobsPerHour"], pikiplexFilterTime, "Pikiplex", "WorkloadMass", [24],
     [2009, 4, 9, 11, 5, 22], 'DE'],
    # [openTimeSeriesJson.MassJobsRICC["massJobsPerHour"], riccFilterTime, "Ricc", "WorkloadMass", [24]],
    # [openTimeSeriesJson.MassJobsUniLuGaia["massJobsPerHour"], uniLuGaiaFilterTime, "UniLuGaia", "WorkloadMass", [24]],
    [openTimeSeriesJson.NumberJobsAnlIntrepid["numberJobsPerHour"], anlIntrepidFilterTime, "AnlIntrepid",
     "NumberOfJobs", [24, 168], [2009, 1, 5, 0, 0, 24], 'US'],
    [openTimeSeriesJson.NumberJobsCiematEuler["numberJobsPerHour"], ciematEulerFilterTime, "CiematEuler",
     "NumberOfJobs", [24, 730, 1460], [2008, 11, 17, 14, 1, 47], 'ES'],
    [openTimeSeriesJson.NumberJobsMetacentrum["numberJobsPerHour"], metacentrumFilterTime, "Metacentrum",
     "NumberOfJobs", [24], [2013, 1, 1, 0, 0, 6], 'CZ'],
    [openTimeSeriesJson.NumberJobsMetacentrumFiltered["numberJobsPerHour"], metacentrumFilteredFilterTime,
     "MetacentrumFiltered", "NumberOfJobs", [24], [2013, 1, 1, 0, 0, 6], 'CZ'],
    [openTimeSeriesJson.NumberJobsPIKIPLEX["numberJobsPerHour"], pikiplexFilterTime, "Pikiplex", "NumberOfJobs", [],
     [2009, 4, 9, 11, 5, 22], 'DE']
    # [openTimeSeriesJson.NumberJobsRICC["numberJobsPerHour"], riccFilterTime, "Ricc", "NumberOfJobs", [24, 168]],
    # [openTimeSeriesJson.NumberJobsUniLuGaia["numberJobsPerHour"], uniLuGaiaFilterTime, "UniLuGaia", "NumberOfJobs", [24]]
]

if __name__ == '__main__':
    execution(executionList[12:], steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData)
