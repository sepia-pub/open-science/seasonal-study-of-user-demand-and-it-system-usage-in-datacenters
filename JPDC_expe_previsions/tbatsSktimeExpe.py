from sktime.forecasting import tbats
import json
import matplotlib.pyplot as plt
import numpy as np
import time
import openTimeSeriesJson

plt.rc('font', size=35)
plt.rcParams['figure.constrained_layout.use'] = True

def filterTime(series, filterTimeInSec):
    seriesFiltered = []
    if filterTimeInSec > 0:
        for value in series[int((filterTimeInSec // 3600) + 1):-int((filterTimeInSec // 3600) + 1)]:
            seriesFiltered.append(value)
    else:
        for value in series:
            seriesFiltered.append(value)
    print("Number of observations =", len(seriesFiltered))
    return seriesFiltered

def createTBATSdict(timeSeries, filter, workloadName, timeSeriesName, steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData, seasons):
    TBATSdict = dict()
    TBATSdict["Workload"] = workloadName
    TBATSdict["TimeSeries"] = timeSeriesName
    TBATSdict["NumberObs"] = len(timeSeries)
    TBATSdict["Filter"] = "[int(("  + str(filter) + "// 3600) + 1):-int((" + str(filter) + "// 3600) + 1)]"
    TBATSdict["StepsPrevision"] = steps
    TBATSdict["NumberPrevisions"] = numberOfPrevisions
    TBATSdict["GapBetweenPrevisionsHours"] = gapBetweenPrevisionsHours
    TBATSdict["LenTrainData"] = lenTrainData
    TBATSdict["Seasons"] = str(seasons)

    return TBATSdict


def computeTBATS(timeSeries, numberOfPrevisions, TBATSdict, seasonesDetected):
    MAE = []
    RMSE = []
    Time = []
    REAL = []
    PREDICT = []
    UPPER = []
    LOWER = []

    for day in range(numberOfPrevisions):
        print("\nDay =", day)

        y_p = np.asarray(timeSeries)[day * 24:2000 + steps + day * 24]

        print("len(y_p) =", len(y_p))

        y_to_train = y_p[:(len(y_p) - steps)]
        y_to_predict = y_p[(len(y_p) - steps):]

        print("len(y_to_train) =", len(y_to_train))
        print("len(y_to_predict) =", len(y_to_predict))

        t0 = time.time()

        estimator = tbats.TBATS(
            sp=seasonesDetected,
            use_arma_errors=True,  # will try models with and without ARMA errors
            use_box_cox=True,  # will try models with Box-Cox transformation and without it
            use_trend=True,  # will try models with trend and without it
            use_damped_trend=True,  # will try models with damping and without it
            show_warnings=False,  # will not be showing any warnings for chosen model
        )
        fitted_model = estimator.fit(y_to_train)

        t1 = time.time()
        dt = t1 - t0
        print('dt =', dt)

        y_for = estimator.predict(fh=list(range(1, steps + 1)))

        U = estimator.predict_interval(coverage=0.95)
        U.columns = U.columns.droplevel(0)
        U.columns = U.columns.droplevel(0)

        yhat_upper = U['upper'].tolist()
        yhat_lower = U['lower'].tolist()

        yhat_upper_adjusted = []
        for val in yhat_upper:
            yhat_upper_adjusted.append(int(val))

        yhat_lower_adjusted = []
        for val in yhat_lower:
            if val < 0:
                yhat_lower_adjusted.append(0)
            else:
                yhat_lower_adjusted.append(int(val))

        y_forecasted = []
        for l in y_for:
            for val in l:
                y_forecasted.append(int(val))

        mae = float(np.mean(np.abs(y_to_predict - y_forecasted)))
        rmse = float(np.sqrt(np.mean((y_to_predict - y_forecasted) ** 2)))
        print('MAE', mae)
        print('RMSE', rmse)

        y_to_predict_adjusted = []
        for val in y_to_predict:
            y_to_predict_adjusted.append(int(val))

        MAE.append(mae)
        RMSE.append(rmse)
        Time.append(dt)
        REAL.append(y_to_predict_adjusted)
        PREDICT.append(y_forecasted)
        UPPER.append(yhat_upper_adjusted)
        LOWER.append(yhat_lower_adjusted)

    TBATSdict["MAE"] = MAE
    TBATSdict["RMSE"] = RMSE
    TBATSdict["Time"] = Time
    TBATSdict["Real"] = REAL
    TBATSdict["Predict"] = PREDICT
    TBATSdict["Upper"] = UPPER
    TBATSdict["Lower"] = LOWER

    return TBATSdict

def jsonResultsTBATS(fileName, TBATSdict):
    with open(fileName, 'w', encoding='utf-8') as f:
        json.dump(TBATSdict, f, indent=4)

def execution(List, steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData):
    for l in List:
        print(l[2])
        print(l[3])
        seasons = str(l[4])
        X = filterTime(l[0], l[1])
        TBATSdict = createTBATSdict(X, l[1], l[2], l[3], steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData, seasons)
        TBATSdict = computeTBATS(X, numberOfPrevisions, TBATSdict, l[4])
        fileName = "tbatsResults_" + str(numberOfPrevisions) + "_Days_" + str(l[2]) + "_" + str(l[3])
        jsonResultsTBATS(fileName, TBATSdict)
        print("\n")

steps = 72
numberOfPrevisions = 40
gapBetweenPrevisionsHours = 24
lenTrainData = 2000

anlIntrepidFilterTime = 2310788
ciematEulerFilterTime = 10890693
metacentrumFilterTime = 18163288
metacentrumFilteredFilterTime = 18163288
pikiplexFilterTime = 5441647
riccFilterTime = 4451683
uniLuGaiaFilterTime = 1800014

executionList = [
    [openTimeSeriesJson.AllocatedCoresSystemAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "AnlIntrepid", "AllocatedCoresSystem", [24, 84, 168]],
    [openTimeSeriesJson.AllocatedCoresSystemCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "CiematEuler", "AllocatedCoresSystem", [24, 168]],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum", "AllocatedCoresSystem", [24, 168]],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "AllocatedCoresSystem", [168]],
    [openTimeSeriesJson.AllocatedCoresSystemPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex", "AllocatedCoresSystem", [24, 168]],
    # [openTimeSeriesJson.AllocatedCoresSystemRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc", "AllocatedCoresSystem", []],
    # [openTimeSeriesJson.AllocatedCoresSystemUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLuGaia", "AllocatedCoresSystem", [168]],
    [openTimeSeriesJson.AllocatedCoresAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "AnlIntrepid", "AllocatedCoresUsers", [24, 168]],
    [openTimeSeriesJson.AllocatedCoresCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "CiematEuler", "AllocatedCoresUsers", [24]],
    [openTimeSeriesJson.AllocatedCoresMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum", "AllocatedCoresUsers", [24, 168]],
    [openTimeSeriesJson.AllocatedCoresMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "AllocatedCoresUsers", [24, 168]],
    [openTimeSeriesJson.AllocatedCoresPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex", "AllocatedCoresUsers", [24, 168]],
    # [openTimeSeriesJson.AllocatedCoresRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc", "AllocatedCoresUsers", [24, 168]],
    # [openTimeSeriesJson.AllocatedCoresUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLuGaia", "AllocatedCoresUsers", [24, 168]],
    [openTimeSeriesJson.MassJobsAnlIntrepid["massJobsPerHour"], anlIntrepidFilterTime, "AnlIntrepid", "WorkloadMass", [24, 168]],
    [openTimeSeriesJson.MassJobsCiematEuler["massJobsPerHour"], ciematEulerFilterTime, "CiematEuler", "WorkloadMass", [24]],
    [openTimeSeriesJson.MassJobsMetacentrum["massJobsPerHour"], metacentrumFilterTime, "Metacentrum", "WorkloadMass", [24]],
    [openTimeSeriesJson.MassJobsMetacentrumFiltered["massJobsPerHour"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "WorkloadMass", []],
    [openTimeSeriesJson.MassJobsPIKIPLEX["massJobsPerHour"], pikiplexFilterTime, "Pikiplex", "WorkloadMass", [24]],
    # [openTimeSeriesJson.MassJobsRICC["massJobsPerHour"], riccFilterTime, "Ricc", "WorkloadMass", [24]],
    # [openTimeSeriesJson.MassJobsUniLuGaia["massJobsPerHour"], uniLuGaiaFilterTime, "UniLuGaia", "WorkloadMass", [24]],
    [openTimeSeriesJson.NumberJobsAnlIntrepid["numberJobsPerHour"], anlIntrepidFilterTime, "AnlIntrepid", "NumberOfJobs", [24, 168]],
    [openTimeSeriesJson.NumberJobsCiematEuler["numberJobsPerHour"], ciematEulerFilterTime, "CiematEuler", "NumberOfJobs", [24]],
    [openTimeSeriesJson.NumberJobsMetacentrum["numberJobsPerHour"], metacentrumFilterTime, "Metacentrum", "NumberOfJobs", [24]],
    [openTimeSeriesJson.NumberJobsMetacentrumFiltered["numberJobsPerHour"], metacentrumFilteredFilterTime, "MetacentrumFiltered", "NumberOfJobs", [24]],
    [openTimeSeriesJson.NumberJobsPIKIPLEX["numberJobsPerHour"], pikiplexFilterTime, "Pikiplex", "NumberOfJobs", []]
    # [openTimeSeriesJson.NumberJobsRICC["numberJobsPerHour"], riccFilterTime, "Ricc", "NumberOfJobs", [24, 168]],
    # [openTimeSeriesJson.NumberJobsUniLuGaia["numberJobsPerHour"], uniLuGaiaFilterTime, "UniLuGaia", "NumberOfJobs", [24]]
]

if __name__ == '__main__':
    execution(executionList, steps, numberOfPrevisions, gapBetweenPrevisionsHours, lenTrainData)


