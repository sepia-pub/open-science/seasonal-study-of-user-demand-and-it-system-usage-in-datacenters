#### **"Seasonal study of user demand and IT system usage in datacenters" artifact**



**Brief presentation:**

This directory contains the data and python files used to obtain the results of the study on the characterization of the seasonality of different time series from 6 HPC workloads and from the Wikipedia workload. The directory consists of the following elements:

- The "hpc_case" folder contains the files and data needed to obtain the seasonality characterization results for the ANL, Ciemat Euler, Metacentrum, PIK IPLEX, RICC and UniLu Gaia workloads. This file concerns ICPADS and JPDC;

- The "wikipedia_2013_case" folder contains the files and data needed to obtain the seasonality characterization results for the wikipedia workload. This folder concerns ICPADS and JPDC;

- The "characterizationUseCase.py" file allows you to run and obtain the results of the characterization use case example. This file only concerns ICPADS;

- The "JPDC_expe_previsions" folder contains the files and data needed to obtain the prevision results for the HPC workloads. This folder only concerns JPDC.

Python files were run using python 3.



**Packages needed for the python files in the JPDC_expe_previsions folder:**

- numpy
- pandas
- json
- tensorflow (to use the LSTM and BiLSTM forecasting method)
- matplotlib
- datetime
- time
- prophet (to use the Prophet forecasting method)
- sklearn (to use the SVR forecasting method)
- sktime (to use the TBATS forecasting method)



**Packages needed for the other files and folders:**

- pandas
- prophet (to use the Prophet forecasting method)
- datetime
- json
- matplotlib
- numpy
- sktime (to use the TBATS forecasting method)
- scipy (to use the periodogram, the Kruskal-Wallis test and the gaussian_kde method)
- scikit_posthocs (to use the Conover-Iman test)
- statsmodels (to use the ADF test, the KPSS test and the LOWESS, MSTL and STL methods)



**In the "hpc_case" folder:**

- The "time_series_json_hpc_data" folder contains workload data;
- The "openTimeSeriesJson.py" file is used to load workload data;

- Run the "periodograms.py" file to generate periodogram figures of the different workloads and time series and obtain the related results. You can disable periodogram execution for one or more time series by commenting out the corresponding lines in the executionList object;
- Run the "conover_iman_and_kruskal_groups.py" file to generate cluster figures of the different workloads and time series and obtain the related results. You can disable cluster computation for one or more time series by commenting out the corresponding lines in the executionList object;
- Run the "seasonal_peaks.py" file to generate seasonal peaks figures. You can disable the computation of seasonal peaks for one or more time series by commenting out the corresponding lines in the executionList object



**In the "wikipedia_2013_case" folder:**

- The "Wikipedia2013.json" file corresponds to wikipedia data for 2013;

- Run the "WikipediaPeriodogram.py" file to generate periodogram figures of the Wikipedia time series and obtain the related results;
- Run the "WikipediaKruskalConoverCluster.py" file to generate cluster figures of the Wikipedia time series and obtain the related results;
- Run the "WikipediaSeasonalPeaks.py" file to generate seasonal peaks figures.



**In the "JPDC_expe_previsions" folder:**

- The "timeSeriesJsonHPC" folder contains all the time series data (json) derived from the HPC workloads;
- The "BiLSTMResultsJson", "LSTMResultsJson", "ProphetResultsJson", "SVRResultsJson" and "TbatsResultsJson" folders contain all the result data (json);
- Run the "displayResults.py" file to generate all the figures and the results of the forecasting methods;
- Run the "BiLSTMExpe.py", "LSTMExpe.py", "prophetExpe.py", "SVRExpe.py" and "tbatsSktimeExpe.py" files to run the forecasting methods and generate the file results (json).
