import pandas as pd
from prophet import Prophet
import datetime as dt
import json
import matplotlib.pyplot as plt
import numpy as np
from sktime.forecasting import tbats

plt.rc('font', size=35)
plt.rcParams['figure.constrained_layout.use'] = True


# Loading data
filename = "hpc_case/time_series_json_hpc_data/NumberJobsAnlIntrepid.json"
with open(filename, 'r') as file:
    AllocatedCoresAnlIntrepid = json.load(file)

y = []
for i in AllocatedCoresAnlIntrepid["numberJobsPerHour"]:
    y.append(i)

print("Number obs =", len(y))

# Keep only the last 3000 data to train and test the forecasting methods
y = np.asarray(y)[-3000:]

print("len(y) =", len(y))

# number of forecasting steps
steps = 72

y_to_train = y[:(len(y) - steps)]
y_to_predict = y[(len(y) - steps):]

# prophet parameters
base = dt.datetime(year=2009, month=9, day=1, hour=23, minute=42, second=6) # Date de fin du workload
hours = [base - dt.timedelta(hours=x) for x in range(len(y))]

df = pd.DataFrame(list(zip(hours, y)), columns=["ds", "y"])


### tbats


estimator = tbats.TBATS(
    sp=[24, 168],
    use_arma_errors=True,  # will try models with and without ARMA errors
    use_box_cox=True,  # will try models with Box-Cox transformation and without it
    use_trend=True,  # will try models with trend and without it
    use_damped_trend=True,  # will try models with damping and without it
    show_warnings=False,  # will not be showing any warnings for chosen model
)
fitted_model = estimator.fit(y_to_train)


### prophet


m = Prophet(seasonality_mode = 'additive',
            daily_seasonality=True,
            weekly_seasonality=True,
            yearly_seasonality=False)
m.fit(df[:-steps])
future = m.make_future_dataframe(periods = steps, freq = 'H')
future['floor'] = 0
forecast = m.predict(future)


### results


y_for_tbats = estimator.predict(fh=list(range(1, steps + 1)))

U = estimator.predict_interval(coverage=0.95)
U.columns = U.columns.droplevel(0)
U.columns = U.columns.droplevel(0)

yhat_upper_tbats = U['upper'].tolist()
yhat_lower_tbats = U['lower'].tolist()

yhat_lower_adjusted_tbats = []
for val in yhat_lower_tbats:
    if val < 0:
        yhat_lower_adjusted_tbats.append(0)
    else:
        yhat_lower_adjusted_tbats.append(val)

y_forecasted_tbats = []
for l in y_for_tbats:
    for val in l:
        y_forecasted_tbats.append(val)

print('MAE tbats', np.mean(np.abs(y_to_predict - y_forecasted_tbats)))
print('RMSE tbats', np.sqrt(np.mean((y_to_predict - y_forecasted_tbats) ** 2)))

t = np.array(range(0, len(y)))

y_to_train = y[:(len(y) - steps)]
y_to_predict = y[(len(y) - steps):]
y_hat_prophet = forecast['yhat'].tolist()[:len(y) - steps]
y_forecasted_prophet = forecast['yhat'].tolist()[len(y) - steps:]
yhat_lower_prophet = forecast['yhat_lower'].tolist()[len(y) - steps:]
yhat_upper_prophet = forecast['yhat_upper'].tolist()[len(y) - steps:]

yhat_lower_adjusted_prophet = []
for val in yhat_lower_prophet:
    if val < 0:
        yhat_lower_adjusted_prophet.append(0)
    else:
        yhat_lower_adjusted_prophet.append(val)

print('MAE prophet', np.mean(np.abs(y_to_predict - yhat_lower_adjusted_prophet)))
print('RMSE prophet', np.sqrt(np.mean((y_to_predict - yhat_lower_adjusted_prophet) ** 2)))

fig, ax = plt.subplots()

ax.plot(t[0:len(y) - steps], y_to_train, color='b', alpha=0.4, label="Training data")
ax.plot(t[len(y) - steps:len(y)], y_to_predict, color='g', alpha=0.4, label="Real data")
ax.plot(t[len(y) - steps:len(y)], y_forecasted_prophet, color='r', label="Prophet prevision")
ax.plot(t[len(y) - steps:], yhat_upper_prophet, color='r', alpha=0.4, label="95% confidence interval prophet")
ax.plot(t[len(y) - steps:], yhat_lower_adjusted_prophet, color='r', alpha=0.4)
ax.fill_between(t[len(y) - steps:], yhat_upper_prophet, yhat_lower_adjusted_prophet, color='r', alpha=0.1)

ax.plot(t[len(y) - steps:len(y)], y_forecasted_tbats, color='orange', label="TBATS prevision")
ax.plot(t[len(y) - steps:], yhat_upper_tbats, color='orange', alpha=0.4, label="95% confidence interval TBATS")
ax.plot(t[len(y) - steps:], yhat_lower_adjusted_tbats, color='orange', alpha=0.4)
ax.fill_between(t[len(y) - steps:], yhat_upper_tbats, yhat_lower_adjusted_tbats, color='orange', alpha=0.1)

ax.set_xlim(-5, len(y) + 5)
ax.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
ax.set_xlabel('Time (hour)', fontsize=45)
ax.set_ylabel('Nb. of requested\ncores', fontsize=45)
ax.tick_params(axis="both", labelsize=40)
ax.legend(fontsize="18")

plt.show()
