import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st

import openTimeSeriesJson

import scikit_posthocs as sp

from statsmodels.nonparametric.smoothers_lowess import lowess

plt.rc('font', size=20)
plt.rcParams['figure.constrained_layout.use'] = True


### Functions


def nb_pairs_conover_iman(X, pval): # To compute the number of sample pairs for which the conover-iman test is not significant
    nb_pval_inf = 0
    nb_pval_to_test = ((len(X) - 1) * len(X)) / 2
    results_conover_iman = sp.posthoc_conover(X)
    for row in range(len(X) - 1):
        for col in range(row + 2, len(X) + 1):
            if results_conover_iman.iloc[row][col] < pval:
                nb_pval_inf += 1
    nb_pval_inf_percent = nb_pval_inf / nb_pval_to_test * 100
    return nb_pval_inf_percent

def nb_groups_kruskal_wallis(X, pval): # To compute the number of clusters
    sample = [i for i in range(len(X))]
    groups = []
    while sample:
        group = [sample[0]]
        for val in sample[1:]:
            newIte = False
            for i in group:
                if X[val] == X[i]:
                    newIte = True
                    break
            if newIte:
                group.append(val)
                continue
            group.append(val)
            arg = [np.array(X[i]) for i in group]
            test_result = st.kruskal(*arg)
            if test_result[1] < pval:
                del group[-1]
        groups.append(group)
        sample = list(set(sample) - set(group))
    return groups

def getGroups(series, period, alpha):
    nb_periods = len(series) // period
    print("Number of samples:", nb_periods)

    samples = [series[i * period:(i + 1) * period] for i in range(nb_periods)]
    percent = nb_pairs_conover_iman(samples, alpha)
    print(percent)

    samples = [series[i * period:(i + 1) * period] for i in range(nb_periods)]
    groups = nb_groups_kruskal_wallis(samples, alpha)
    print("Number of clusters:", len(groups))
    lenGroup = []
    for g in groups:
        lenGroup.append(round(len(g) / nb_periods * 100, 2))
    lenGroup = sorted(lenGroup, reverse=True)
    print("percentage of samples in the clusters:", lenGroup)

    return groups, samples

def filterTime(series, filterTimeInSec):
    seriesFiltered = []
    for value in series[int((filterTimeInSec // 3600) + 1):-int((filterTimeInSec // 3600) + 1)]:
        seriesFiltered.append(value)
    print("Number of observations =", len(seriesFiltered))
    return seriesFiltered

def plotClusters(groups, samples):
    for l in groups:
        data = []
        for val in l:
            data += samples[val]
        density_function = st.gaussian_kde(data, bw_method=0.3)
        x = np.linspace(min(data), max(data))
        plt.plot(x, density_function(x))
    plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
    # plt.xlabel('Number of cores', fontsize=28)
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    plt.show()

def removeTrend(series, granularity): # Removing the trend component from the time series if it is present
    x_abs = [i for i in range(len(series))]
    low = lowess(series, x_abs, frac=granularity)
    low = [v[1] for v in low]
    series_without_trend = [val - tr for val, tr in zip(series, low)]
    return series_without_trend

def execution(List): # To obtain the number of clusters depending on the observed seasonality
    for l in List:
        print(l[2])
        X = filterTime(l[0], l[1])
        if l[5] == "T":
            X = removeTrend(X, l[6])
        if "D" in l[3]:
            print("day")
            groupsDay, samplesDay = getGroups(X, 24, alphaUsed)
            if "D" not in l[4]:
                plotClusters(groupsDay, samplesDay)
            print("\n")
        if "H" in l[3]:
            print("half-week")
            groupsHalfWeek, samplesHalfWeek = getGroups(X, 84, alphaUsed)
            if "H" not in l[4]:
                plotClusters(groupsHalfWeek, samplesHalfWeek)
            print("\n")
        if "W" in l[3]:
            print("week")
            groupsWeek, samplesWeek = getGroups(X, 168, alphaUsed)
            if "W" not in l[4]:
                plotClusters(groupsWeek, samplesWeek)
            print("\n")
        if "M" in l[3]:
            print("month")
            groupsMonth, samplesMonth = getGroups(X, 730, alphaUsed)
            if "M" not in l[4]:
                plotClusters(groupsMonth, samplesMonth)
            print("\n")
        if "T" in l[3]:
            print("Two months")
            groupsTwoMonths, samplesTwoMonths = getGroups(X, 1460, alphaUsed)
            if "T" not in l[4]:
                plotClusters(groupsTwoMonths, samplesTwoMonths)
            print("\n")
        print("\n")


alphaUsed = 0.05 # For Kruskal-Wallis and Conover-Iman tests

# Number of seconds to subtract at the start and the end of the time series
anlIntrepidFilterTime = 2310788
ciematEulerFilterTime = 10890693
metacentrumFilterTime = 18163288
metacentrumFilteredFilterTime = 18163288
pikiplexFilterTime = 5441647
riccFilterTime = 4451683
uniLuGaiaFilterTime = 1800014

# Each list correspond to the execution of one time series:

# The first item loads the time series;

# The second item indicates the filter to be applied (second);

# The third item is the definition of the time series;

# The fourth item indicates the seasonality of the time series;

# The fifth item indicates the seasonality for which clusters are shown (note that this does not work in all cases);

# The sixth item indicates if the time series requires differentiation (D), has a trend component to be removed (T),
# or nothing to be done to make it stationary (N);

# The seventh item is given when removing the trend component is necessary. It is used to set a smoothing
# parameter for the LOWESS function.

executionList = [
    [openTimeSeriesJson.AllocatedCoresSystemAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "Anl Intrepid Allocated cores system", "DWH", "D", "T", 0.2],
    # [openTimeSeriesJson.AllocatedCoresSystemCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "Ciemat Euler Allocated cores system", "DW", "", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum Allocated cores system", "DWM", "", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "Metacentrum Filtered Allocated cores system", "WM", "", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresSystemPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex Allocated cores system", "DWT", "", "T", 0.1],
    [openTimeSeriesJson.AllocatedCoresSystemRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc Allocated cores system", "", "", "N"],
    [openTimeSeriesJson.AllocatedCoresSystemUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLu Gaia Allocated cores system", "W", "", "N"],

    [openTimeSeriesJson.AllocatedCoresAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "Anl Intrepid Allocated cores", "DW", "D", "N"],
    # [openTimeSeriesJson.AllocatedCoresCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "Ciemat Euler Allocated cores", "D", "D", "N"],
    [openTimeSeriesJson.AllocatedCoresMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum Allocated cores", "DWM", "", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "Metacentrum Filtered Allocated cores", "DWM", "", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex Allocated cores", "DW", "", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc Allocated cores", "DW", "", "N"],
    [openTimeSeriesJson.AllocatedCoresUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLu Gaia Allocated cores", "DW", "", "N"],

    [openTimeSeriesJson.MassJobsAnlIntrepid["massJobsPerHour"], anlIntrepidFilterTime, "Anl Intrepid Workload mass", "DW", "D", "N"],
    [openTimeSeriesJson.MassJobsCiematEuler["massJobsPerHour"], ciematEulerFilterTime, "Ciemat Euler Workload mass", "D", "", "N"],
    [openTimeSeriesJson.MassJobsMetacentrum["massJobsPerHour"], metacentrumFilterTime, "Metacentrum Workload mass", "D", "", "N"],
    [openTimeSeriesJson.MassJobsMetacentrumFiltered["massJobsPerHour"], metacentrumFilteredFilterTime, "Metacentrum Filtered Workload mass", "", "", "N"],
    [openTimeSeriesJson.MassJobsPIKIPLEX["massJobsPerHour"], pikiplexFilterTime, "Pikiplex Workload mass", "D", "", "N"],
    [openTimeSeriesJson.MassJobsRICC["massJobsPerHour"], riccFilterTime, "Ricc Workload mass", "D", "", "N"],
    [openTimeSeriesJson.MassJobsUniLuGaia["massJobsPerHour"], uniLuGaiaFilterTime, "UniLu Gaia Workload mass", "D", "", "N"],

    [openTimeSeriesJson.NumberJobsAnlIntrepid["numberJobsPerHour"], anlIntrepidFilterTime, "Anl Intrepid Number of jobs", "DW", "", "T", 0.2],
    # [openTimeSeriesJson.NumberJobsCiematEuler["numberJobsPerHour"], ciematEulerFilterTime, "Ciemat Euler Number of jobs", "DMT", "", "T", 0.025],
    [openTimeSeriesJson.NumberJobsMetacentrum["numberJobsPerHour"], metacentrumFilterTime, "Metacentrum Number of jobs", "D", "", "T", 0.2],
    [openTimeSeriesJson.NumberJobsMetacentrumFiltered["numberJobsPerHour"], metacentrumFilteredFilterTime, "Metacentrum Filtered Number of jobs", "D", "", "T", 0.2],
    [openTimeSeriesJson.NumberJobsPIKIPLEX["numberJobsPerHour"], pikiplexFilterTime, "Pikiplex Number of jobs", "", "", "N"],
    [openTimeSeriesJson.NumberJobsRICC["numberJobsPerHour"], riccFilterTime, "Ricc Number of jobs", "DW", "", "N"],
    [openTimeSeriesJson.NumberJobsUniLuGaia["numberJobsPerHour"], uniLuGaiaFilterTime, "UniLu Gaia Number of jobs", "D", "", "T", 0.05]
]


execution(executionList)
