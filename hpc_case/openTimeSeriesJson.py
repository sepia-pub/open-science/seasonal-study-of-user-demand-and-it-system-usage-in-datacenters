import json

### Maximum number of allocated cores by the system

filename = "time_series_json_hpc_data/AllocatedCoresSystemAnlIntrepid.json"
with open(filename, 'r') as file:
    AllocatedCoresSystemAnlIntrepid = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresSystemCiematEuler.json"
with open(filename, 'r') as file:
    AllocatedCoresSystemCiematEuler = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresSystemMetacentrum.json"
with open(filename, 'r') as file:
    AllocatedCoresSystemMetacentrum = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresSystemMetacentrumFiltered.json"
with open(filename, 'r') as file:
    AllocatedCoresSystemMetacentrumFiltered = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresSystemPIKIPLEX.json"
with open(filename, 'r') as file:
    AllocatedCoresSystemPIKIPLEX = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresSystemRICC.json"
with open(filename, 'r') as file:
    AllocatedCoresSystemRICC = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresSystemUniLuGaia.json"
with open(filename, 'r') as file:
    AllocatedCoresSystemUniLuGaia = json.load(file)

### Maximum number of requested cores by the users

filename = "time_series_json_hpc_data/AllocatedCoresAnlIntrepid.json"
with open(filename, 'r') as file:
    AllocatedCoresAnlIntrepid = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresCiematEuler.json"
with open(filename, 'r') as file:
    AllocatedCoresCiematEuler = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresMetacentrum.json"
with open(filename, 'r') as file:
    AllocatedCoresMetacentrum = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresMetacentrumFiltered.json"
with open(filename, 'r') as file:
    AllocatedCoresMetacentrumFiltered = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresPIKIPLEX.json"
with open(filename, 'r') as file:
    AllocatedCoresPIKIPLEX = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresRICC.json"
with open(filename, 'r') as file:
    AllocatedCoresRICC = json.load(file)

filename = "time_series_json_hpc_data/AllocatedCoresUniLuGaia.json"
with open(filename, 'r') as file:
    AllocatedCoresUniLuGaia = json.load(file)

### Workload mass

filename = "time_series_json_hpc_data/MassJobsAnlIntrepid.json"
with open(filename, 'r') as file:
    MassJobsAnlIntrepid = json.load(file)

filename = "time_series_json_hpc_data/MassJobsCiematEuler.json"
with open(filename, 'r') as file:
    MassJobsCiematEuler = json.load(file)

filename = "time_series_json_hpc_data/MassJobsMetacentrum.json"
with open(filename, 'r') as file:
    MassJobsMetacentrum = json.load(file)

filename = "time_series_json_hpc_data/MassJobsMetacentrumFiltered.json"
with open(filename, 'r') as file:
    MassJobsMetacentrumFiltered = json.load(file)

filename = "time_series_json_hpc_data/MassJobsPIKIPLEX.json"
with open(filename, 'r') as file:
    MassJobsPIKIPLEX = json.load(file)

filename = "time_series_json_hpc_data/MassJobsRICC.json"
with open(filename, 'r') as file:
    MassJobsRICC = json.load(file)

filename = "time_series_json_hpc_data/MassJobsUniLuGaia.json"
with open(filename, 'r') as file:
    MassJobsUniLuGaia = json.load(file)

### Number of jobs

filename = "time_series_json_hpc_data/NumberJobsAnlIntrepid.json"
with open(filename, 'r') as file:
    NumberJobsAnlIntrepid = json.load(file)

filename = "time_series_json_hpc_data/NumberJobsCiematEuler.json"
with open(filename, 'r') as file:
    NumberJobsCiematEuler = json.load(file)

filename = "time_series_json_hpc_data/NumberJobsMetacentrum.json"
with open(filename, 'r') as file:
    NumberJobsMetacentrum = json.load(file)

filename = "time_series_json_hpc_data/NumberJobsMetacentrumFiltered.json"
with open(filename, 'r') as file:
    NumberJobsMetacentrumFiltered = json.load(file)

filename = "time_series_json_hpc_data/NumberJobsPIKIPLEX.json"
with open(filename, 'r') as file:
    NumberJobsPIKIPLEX = json.load(file)

filename = "time_series_json_hpc_data/NumberJobsRICC.json"
with open(filename, 'r') as file:
    NumberJobsRICC = json.load(file)

filename = "time_series_json_hpc_data/NumberJobsUniLuGaia.json"
with open(filename, 'r') as file:
    NumberJobsUniLuGaia = json.load(file)