import openTimeSeriesJson

import matplotlib.pyplot as plt
from scipy.signal import periodogram
from statsmodels.tsa.stattools import adfuller, kpss
from statsmodels.nonparametric.smoothers_lowess import lowess

plt.rc('font', size=35)
plt.rcParams['figure.constrained_layout.use'] = True


### Functions


def filterTime(series, filterTimeInSec):
    seriesFiltered = []
    for value in series[int((filterTimeInSec // 3600) + 1):-int((filterTimeInSec // 3600) + 1)]:
        seriesFiltered.append(value)
    print("Number of observations =", len(seriesFiltered))
    return seriesFiltered

def differentiateSeries(series): # Differentiating a non-stationary time series with no trend
    print("A differentiation of the time series is necessary. There is no trend\n")
    seriesDiff = []
    for i in range(len(series) - 1):
        seriesDiff.append(series[i] - series[i + 1])
    return seriesDiff

def printPeriodogram(series): # Show periodogram and top 8 frequencies in time series.

    print("Displaying the periodogram of the time series\n")

    # Periodogram
    freqencies, spectrum = periodogram(series)
    plt.plot(freqencies, spectrum, color='blue')
    plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
    # plt.title("Periodogram of the time series")
    plt.xlabel('Frequency (Hz)', fontsize=45)
    plt.ylabel('Power spectral density', fontsize=45)
    plt.xticks(fontsize=40)
    plt.yticks(fontsize=40)
    plt.show()

    # Top 8 frequencies
    values = []
    for i, j in zip(list(spectrum), list(freqencies)):
        if j != 0:
            values.append([i, j, 1 / j])
        else:
            values.append([i, j, 1e10])
    values.sort(reverse=True)
    for val in values[0:8]:
        print(val)

def stationaryTestsResults(series, alpha): # Assessing the stationarity of a time series using ADF and KPSS tests
    print("Testing the stationarity of the time series")

    # ADF test
    testAdf = adfuller(series)
    # p-value
    print("The p-value ADF:", testAdf[1])

    # KPSS test
    testKpss = kpss(series)
    # p-value
    print("The p-value KPSS:", testKpss[1])

    if testAdf[1] <= alpha <= testKpss[1]:
        print("The time series is stationary\n")

    else:
        print("The time series is not stationary\n")

def removeTrend(series, granularity): # Removing the trend component from the time series if it is present
    print("A trend component is present")
    print("Displaying the time series with trend\n")
    x_abs = [i for i in range(len(series))]
    low = lowess(series, x_abs, frac=granularity)
    low = [v[1] for v in low]

    plt.plot(series)
    plt.title("Time series with trend")
    plt.plot(low)
    plt.xlabel("time (hour)")
    plt.show()

    series_without_trend = [val - tr for val, tr in zip(series, low)]

    print("Displaying the time series without its trend\n")

    plt.plot(series_without_trend)
    plt.title("Time series without trend")
    plt.xlabel("time (hour)")
    plt.show()

    return series_without_trend

def plotSeries(series):
    plt.plot(series)
    plt.title("Time series")
    plt.xlabel("time (hour)")
    plt.show()

def execution(List):
    for l in List:
        print("######################################")
        print(l[2])
        X = filterTime(l[0], l[1])
        print("Displaying the time series\n")
        plotSeries(X)
        stationaryTestsResults(X, alphaUsed)
        if l[3] == "T":
            X = removeTrend(X, l[4])
            stationaryTestsResults(X, alphaUsed)
        elif l[3] == "D":
            X = differentiateSeries(X)
            print("Displaying the differentiate time series\n")
            plotSeries(X)
            stationaryTestsResults(X, alphaUsed)
        printPeriodogram(X)
        print("\n")

alphaUsed = 0.05 # For ADF and KPSS tests

# Number of seconds to subtract at the start and the end of the time series
anlIntrepidFilterTime = 2310788
ciematEulerFilterTime = 10890693
metacentrumFilterTime = 18163288
metacentrumFilteredFilterTime = 18163288
pikiplexFilterTime = 5441647
riccFilterTime = 4451683
uniLuGaiaFilterTime = 1800014

# Each list correspond to the execution of one time series:

# The first item loads the time series;

# The second item indicates the filter to be applied (second);

# The third item is the definition of the time series;

# The fourth item indicates if the time series requires differentiation (D), has a trend component to be removed (T),
# or nothing to be done to make it stationary (N);

# The fifth item is given when removing the trend component is necessary. It is used to set a smoothing
# parameter for the LOWESS function.

executionList = [
    [openTimeSeriesJson.AllocatedCoresSystemAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "Anl Intrepid Allocated cores system", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresSystemCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "Ciemat Euler Allocated cores system", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum Allocated cores system", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "Metacentrum Filtered Allocated cores system", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresSystemPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex Allocated cores system", "T", 0.1],
    [openTimeSeriesJson.AllocatedCoresSystemRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc Allocated cores system", "N"],
    [openTimeSeriesJson.AllocatedCoresSystemUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLu Gaia Allocated cores system", "N"],

    [openTimeSeriesJson.AllocatedCoresAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "Anl Intrepid Allocated cores", "N"],
    [openTimeSeriesJson.AllocatedCoresCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "Ciemat Euler Allocated cores", "D"],
    [openTimeSeriesJson.AllocatedCoresMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum Allocated cores", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "Metacentrum Filtered Allocated cores", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex Allocated cores", "T", 0.2],
    [openTimeSeriesJson.AllocatedCoresRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc Allocated cores", "N"],
    [openTimeSeriesJson.AllocatedCoresUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLu Gaia Allocated cores", "N"],

    [openTimeSeriesJson.MassJobsAnlIntrepid["massJobsPerHour"], anlIntrepidFilterTime, "Anl Intrepid Workload mass", "N"],
    [openTimeSeriesJson.MassJobsCiematEuler["massJobsPerHour"], ciematEulerFilterTime, "Ciemat Euler Workload mass", "D"],
    [openTimeSeriesJson.MassJobsMetacentrum["massJobsPerHour"], metacentrumFilterTime, "Metacentrum Workload mass", "D"],
    [openTimeSeriesJson.MassJobsMetacentrumFiltered["massJobsPerHour"], metacentrumFilteredFilterTime, "Metacentrum Filtered Workload mass", "D"],
    [openTimeSeriesJson.MassJobsPIKIPLEX["massJobsPerHour"], pikiplexFilterTime, "Pikiplex Workload mass", "N"],
    [openTimeSeriesJson.MassJobsRICC["massJobsPerHour"], riccFilterTime, "Ricc Workload mass", "N"],
    [openTimeSeriesJson.MassJobsUniLuGaia["massJobsPerHour"], uniLuGaiaFilterTime, "UniLu Gaia Workload mass", "N"],

    [openTimeSeriesJson.NumberJobsAnlIntrepid["numberJobsPerHour"], anlIntrepidFilterTime, "Anl Intrepid Number of jobs", "T", 0.2],
    [openTimeSeriesJson.NumberJobsCiematEuler["numberJobsPerHour"], ciematEulerFilterTime, "Ciemat Euler Number of jobs", "T", 0.025],
    [openTimeSeriesJson.NumberJobsMetacentrum["numberJobsPerHour"], metacentrumFilterTime, "Metacentrum Number of jobs", "T", 0.2],
    [openTimeSeriesJson.NumberJobsMetacentrumFiltered["numberJobsPerHour"], metacentrumFilteredFilterTime, "Metacentrum Filtered Number of jobs", "T", 0.2],
    [openTimeSeriesJson.NumberJobsPIKIPLEX["numberJobsPerHour"], pikiplexFilterTime, "Pikiplex Number of jobs", "D"],
    [openTimeSeriesJson.NumberJobsRICC["numberJobsPerHour"], riccFilterTime, "Ricc Number of jobs", "N"],
    [openTimeSeriesJson.NumberJobsUniLuGaia["numberJobsPerHour"], uniLuGaiaFilterTime, "UniLu Gaia Number of jobs", "T", 0.05]
]


### Results


execution(executionList)

# Anl Intrepid Allocated cores system : 168, 84, 24
# Ciemat Euler Allocated cores system : 168, 24
# Metacentrum Allocated cores system : 730, 168, 24
# Metacentrum Filtered Allocated cores system : 730, 168
# Pikiplex Allocated cores system : 1460, 168, 24
# Ricc Allocated cores system : N
# UniLu Gaia Allocated cores system : 168

# Anl Intrepid Allocated cores : 24, 168
# Ciemat Euler Allocated cores : 24
# Metacentrum Allocated cores : 24, 168, 730
# Metacentrum Filtered Allocated cores : 168, 24, 730
# Pikiplex Allocated cores : 24, 168
# Ricc Allocated cores : 168, 24
# UniLu Gaia Allocated cores : 168, 24

# Anl Intrepid Workload mass : 24, 168
# Ciemat Euler Workload mass : 24
# Metacentrum Workload mass : 24
# Metacentrum Filtered Workload mass : N
# Pikiplex Workload mass : 24
# Ricc Workload mass : 24
# UniLu Gaia Workload mass : 24

# Anl Intrepid Number of jobs : 168, 24
# Ciemat Euler Number of jobs : 24, 730, 1460
# Metacentrum Number of jobs : 24
# Metacentrum Filtered Number of jobs : 24
# Pikiplex Number of jobs : N
# Ricc Number of jobs : 24, 168
# UniLu Gaia Number of jobs : 24