import openTimeSeriesJson
import datetime

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

plt.rc('font', size=20)
plt.rcParams['figure.constrained_layout.use'] = True


### Functions


def find_peaks(series): # Computes the percentage of seasonal peaks based on the time of day or the time of day of the week
    peaks_percent = [0] * len(series[0])
    number_peaks = len(series)
    for period in series:
        indexPeak = period.index(max(period))
        peaks_percent[indexPeak] += 1
    peaks_percent = [val / number_peaks for val in peaks_percent]
    return peaks_percent

def filterTime(series, filterTimeInSec):
    seriesFiltered = [val for val in series[int((filterTimeInSec // 3600) + 1):-int((filterTimeInSec // 3600) + 1)]]
    print("Number of observations =", len(seriesFiltered))
    return seriesFiltered

def plotSeasonalPeaksDay(x, p):
    plt.plot(x, p, color='blue')
    plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
    plt.xlabel('Day (hour)', fontsize=28)
    plt.ylabel('Frequency', fontsize=28)
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    plt.xticks(rotation=45)
    myFmt = mdates.DateFormatter('%H:%M')
    plt.gca().xaxis.set_major_formatter(myFmt)
    plt.show()

def plotAllSeasonalPeaksDay(List):
    for l in List:
        plt.plot(List[0][0], l[1], label=l[2])
    plt.xlabel('Day (hour)', fontsize=28)
    plt.ylabel('Frequency', fontsize=28)
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    plt.xticks(rotation=45)
    plt.legend()
    myFmt = mdates.DateFormatter('%H:%M')
    plt.gca().xaxis.set_major_formatter(myFmt)
    plt.show()

def plotAllSeasonalPeaksWeek(List):
    for l in List:
        plt.plot(List[0][0], l[1], label=l[2])
    plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
    plt.xlabel('Week (hour)', fontsize=28)
    plt.ylabel('Frequency', fontsize=28)
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    plt.xticks(rotation=45)
    plt.legend()
    myFmt = mdates.DateFormatter('%A')
    plt.gca().xaxis.set_major_formatter(myFmt)
    labels_to_print = list(range(0, 168, 12))
    plt.xticks(labels_to_print, [List[0][0][i] for i in labels_to_print])
    plt.show()

def plotSeasonalPeaksWeek(x, p):
    plt.plot(x, p, color='blue')
    plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
    plt.xlabel('Week (hour)', fontsize=28)
    plt.ylabel('Frequency', fontsize=28)
    plt.xticks(fontsize=25)
    plt.yticks(fontsize=25)
    plt.xticks(rotation=45)
    myFmt = mdates.DateFormatter('%A')
    plt.gca().xaxis.set_major_formatter(myFmt)
    labels_to_print = list(range(0, 168, 12))
    plt.xticks(labels_to_print, [x[i] for i in labels_to_print])
    plt.show()

def execution(List):
    results_x_and_peaks = []
    for l in List:
        print(l[2])
        X = filterTime(l[0], l[1])
        if "D" in l[3]:
            print("day")
            nb_days = len(X) // 24
            print("Number of days:", nb_days)
            Y = [X[i * 24:(i + 1) * 24] for i in range(nb_days)]
            x_values = [
                datetime.datetime(year=l[4][0], month=l[4][1], day=l[4][2], hour=l[4][3], minute=l[4][4], second=l[4][5]) + datetime.timedelta(hours=i)
                for i in range(24)]
            peaks = find_peaks(Y)
            peaks = peaks[l[4][6]:] + peaks[:l[4][6]]
            # plotSeasonalPeaksDay(x_values, peaks)
            results_x_and_peaks.append([x_values[:], peaks[:], l[2]])
        if "W" in l[3]:
            print("week")
            nb_weeks = len(X) // 168
            print("Number of weeks:", nb_weeks)
            Y = [X[i * 168:(i + 1) * 168] for i in range(nb_weeks)]
            x_values = [
                datetime.datetime(year=l[5][0], month=l[5][1], day=l[5][2], hour=l[5][3], minute=l[5][4], second=l[5][5]) + datetime.timedelta(hours=i)
                for i in range(168)]
            x_values = [dt.strftime('%A %H:%M') for dt in x_values]
            peaks = find_peaks(Y)
            peaks = peaks[l[5][6]:] + peaks[:l[5][6]]
            # plotSeasonalPeaksWeek(x_values, peaks)
            results_x_and_peaks.append([x_values[:], peaks[:], l[2]])
        print("\n")
    return results_x_and_peaks

# Number of seconds to subtract at the start and the end of the time series
anlIntrepidFilterTime = 2310788
ciematEulerFilterTime = 10890693
metacentrumFilterTime = 18163288
metacentrumFilteredFilterTime = 18163288
pikiplexFilterTime = 5441647
riccFilterTime = 4451683
uniLuGaiaFilterTime = 1800014

# Each list correspond to the execution of one time series:

# The first item loads the time series;

# The second item indicates the filter to be applied (second);

# The third item is the definition of the time series;

# The fourth item indicates the seasonality of the time series (only "D", "W", "DW" or "");

# The fifth item indicates the date to be considered for calibrating peak detection at midnight (if "D" or "DW");

# The sixth item indicates the date to be considered for calibrating peak detection on Monday at midnight (if "W" or "DW").

executionList = [
    [openTimeSeriesJson.AllocatedCoresSystemAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "Anl Intrepid System allocated cores", "DW", [2009, 1, 31, 0, 0, 0, 6], [2009, 2, 2, 0, 0, 0, 30]],
    [openTimeSeriesJson.AllocatedCoresSystemCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "Ciemat Euler System allocated cores", "DW", [2009, 3, 23, 0, 0, 0, 8], [2009, 3, 23, 0, 0, 0, 152]],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum System allocated cores", "DW", [2013, 7, 30, 0, 0, 0, 18], [2013, 7, 30, 0, 0, 0, 138]],
    [openTimeSeriesJson.AllocatedCoresSystemMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "Metacentrum Filtered System allocated cores", "W", [], [2013, 7, 30, 0, 0, 0, 138]],
    [openTimeSeriesJson.AllocatedCoresSystemPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex System allocated cores", "DW", [2009, 6, 11, 0, 0, 0, 13], [2009, 6, 8, 0, 0, 0, 85]],
    [openTimeSeriesJson.AllocatedCoresSystemRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc System allocated cores", "", [], []],
    [openTimeSeriesJson.AllocatedCoresSystemUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLu Gaia System allocated cores", "W", [], [2014, 6, 9, 0, 0, 0, 88]],

    [openTimeSeriesJson.AllocatedCoresAnlIntrepid["jobMaxAllocatedCores"], anlIntrepidFilterTime, "Anl Intrepid Requested cores", "DW", [2009, 1, 31, 0, 0, 0, 6], [2009, 2, 2, 0, 0, 0, 30]],
    [openTimeSeriesJson.AllocatedCoresCiematEuler["jobMaxAllocatedCores"], ciematEulerFilterTime, "Ciemat Euler Requested cores", "D", [2009, 3, 23, 0, 0, 0, 8], []],
    [openTimeSeriesJson.AllocatedCoresMetacentrum["jobMaxAllocatedCores"], metacentrumFilterTime, "Metacentrum Requested cores", "DW", [2013, 7, 30, 0, 0, 0, 18], [2013, 7, 29, 0, 0, 0, 138]],
    [openTimeSeriesJson.AllocatedCoresMetacentrumFiltered["jobMaxAllocatedCores"], metacentrumFilteredFilterTime, "Metacentrum Filtered Requested cores", "DW", [2013, 7, 30, 0, 0, 0, 18], [2013, 7, 30, 0, 0, 0, 138]],
    [openTimeSeriesJson.AllocatedCoresPIKIPLEX["jobMaxAllocatedCores"], pikiplexFilterTime, "Pikiplex Requested cores", "DW", [2009, 6, 11, 0, 0, 0, 13], [2009, 6, 8, 0, 0, 0, 85]],
    [openTimeSeriesJson.AllocatedCoresRICC["jobMaxAllocatedCores"], riccFilterTime, "Ricc Requested cores", "DW", [2010, 6, 21, 0, 0, 0, 11], [2010, 6, 21, 0, 0, 0, 155]],
    [openTimeSeriesJson.AllocatedCoresUniLuGaia["jobMaxAllocatedCores"], uniLuGaiaFilterTime, "UniLu Gaia Requested cores", "DW", [2014, 6, 12, 0, 0, 0, 16], [2014, 6, 9, 0, 0, 0, 88]],

    [openTimeSeriesJson.MassJobsAnlIntrepid["massJobsPerHour"], anlIntrepidFilterTime, "Anl Intrepid Work volume", "DW", [2009, 1, 31, 0, 0, 0, 6], [2009, 2, 2, 0, 0, 0, 30]],
    [openTimeSeriesJson.MassJobsCiematEuler["massJobsPerHour"], ciematEulerFilterTime, "Ciemat Euler Work volume", "D", [2009, 3, 23, 0, 0, 0, 8], []],
    [openTimeSeriesJson.MassJobsMetacentrum["massJobsPerHour"], metacentrumFilterTime, "Metacentrum Work volume", "D", [2013, 7, 30, 0, 0, 0, 18], []],
    [openTimeSeriesJson.MassJobsMetacentrumFiltered["massJobsPerHour"], metacentrumFilteredFilterTime, "Metacentrum Filtered Work volume", "", [], []],
    [openTimeSeriesJson.MassJobsPIKIPLEX["massJobsPerHour"], pikiplexFilterTime, "Pikiplex Work volume", "D", [2009, 6, 11, 0, 0, 0, 13], []],
    [openTimeSeriesJson.MassJobsRICC["massJobsPerHour"], riccFilterTime, "Ricc Work volume", "D", [2010, 6, 21, 0, 0, 0, 11], []],
    [openTimeSeriesJson.MassJobsUniLuGaia["massJobsPerHour"], uniLuGaiaFilterTime, "UniLu Gaia Work volume", "D", [2014, 6, 12, 0, 0, 0, 16], []],

    [openTimeSeriesJson.NumberJobsAnlIntrepid["numberJobsPerHour"], anlIntrepidFilterTime, "Anl Intrepid Number of jobs", "DW", [2009, 1, 31, 0, 0, 0, 6], [2009, 2, 2, 0, 0, 0, 30]],
    [openTimeSeriesJson.NumberJobsCiematEuler["numberJobsPerHour"], ciematEulerFilterTime, "Ciemat Euler Number of jobs", "D", [2009, 3, 23, 0, 0, 0, 8], []],
    [openTimeSeriesJson.NumberJobsMetacentrum["numberJobsPerHour"], metacentrumFilterTime, "Metacentrum Number of jobs", "D", [2013, 7, 30, 0, 0, 0, 18], []],
    [openTimeSeriesJson.NumberJobsMetacentrumFiltered["numberJobsPerHour"], metacentrumFilteredFilterTime, "Metacentrum Filtered Number of jobs", "D", [2013, 7, 30, 0, 0, 0, 18], []],
    [openTimeSeriesJson.NumberJobsPIKIPLEX["numberJobsPerHour"], pikiplexFilterTime, "Pikiplex Number of jobs", "", [], []],
    [openTimeSeriesJson.NumberJobsRICC["numberJobsPerHour"], riccFilterTime, "Ricc Number of jobs", "DW", [2010, 6, 21, 0, 0, 0, 11], [2010, 6, 21, 0, 0, 0, 155]],
    [openTimeSeriesJson.NumberJobsUniLuGaia["numberJobsPerHour"], uniLuGaiaFilterTime, "UniLu Gaia Number of jobs", "D", [2010, 6, 21, 0, 0, 0, 11], []]
]


### Results


results_x_and_peaks = execution(executionList)

allocatedCoresSystemDay = [results_x_and_peaks[i] for i in [0, 2, 4, 7]]
plotAllSeasonalPeaksDay(allocatedCoresSystemDay)

allocatedCoresSystemWeek = [results_x_and_peaks[i] for i in [1, 3, 5, 6, 8, 9]]
plotAllSeasonalPeaksWeek(allocatedCoresSystemWeek)

allocatedCoresDay = [results_x_and_peaks[i] for i in [10, 12, 13, 15, 17, 19, 21]]
plotAllSeasonalPeaksDay(allocatedCoresDay)

allocatedCoresWeek = [results_x_and_peaks[i] for i in [11, 14, 16, 18, 20, 22]]
plotAllSeasonalPeaksWeek(allocatedCoresWeek)

workloadMassDay = [results_x_and_peaks[i] for i in [23, 25, 26, 27, 28, 29]]
plotAllSeasonalPeaksDay(workloadMassDay)

workloadMassWeek = [results_x_and_peaks[i] for i in [24]]
plotAllSeasonalPeaksWeek(workloadMassWeek)

numberOfJobsDay = [results_x_and_peaks[i] for i in [30, 32, 33, 34, 35, 37]]
plotAllSeasonalPeaksDay(numberOfJobsDay)

numberOfJobsWeek = [results_x_and_peaks[i] for i in [31, 36]]
plotAllSeasonalPeaksWeek(numberOfJobsWeek)