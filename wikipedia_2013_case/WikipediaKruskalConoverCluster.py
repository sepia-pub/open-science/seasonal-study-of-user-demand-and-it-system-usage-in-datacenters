import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
from statsmodels.nonparametric.smoothers_lowess import lowess
import json
import scikit_posthocs as sp

plt.rc('font', size=20)
plt.rcParams['figure.constrained_layout.use'] = True


### Functions


def nb_pairs_conover_iman(X, pval): # To compute the number of sample pairs for which the conover-iman test is not significant
    nb_pval_inf = 0
    nb_pval_to_test = ((len(X) - 1) * len(X)) / 2
    results_conover_iman = sp.posthoc_conover(X)
    for row in range(len(X) - 1):  # 0 à 237
        for col in range(row + 2, len(X) + 1):  # 2 à 239
            if results_conover_iman.iloc[row][col] < pval:
                nb_pval_inf += 1
    nb_pval_inf_percent = nb_pval_inf / nb_pval_to_test * 100
    return nb_pval_inf_percent

def nb_groups_kruskal_wallis(X, pval): # To compute the number of clusters
    sample = [i for i in range(len(X))]
    groups = []
    while sample:
        group = [sample[0]]
        for val in sample[1:]:
            newIte = False
            for i in group:
                if X[val] == X[i]:
                    newIte = True
                    break
            if newIte:
                group.append(val)
                continue
            group.append(val)
            arg = [np.array(X[i]) for i in group]
            test_result = st.kruskal(*arg)
            if test_result[1] < pval:
                del group[-1]
        groups.append(group)
        sample = list(set(sample) - set(group))
    return groups

nom_fichier = "Wikipedia2013.json"
with open(nom_fichier, 'r') as file:
    Wikipedia = json.load(file)


### Requests


print("Requests")

X = []
for i in Wikipedia["Requests"]:
    X.append(i)

x_abs = [i for i in range(len(X))]
low = lowess(X, x_abs, frac=0.1)
low = [v[1] for v in low]
series_without_trend = [val - tr for val, tr in zip(X, low)]

# Day

nb_days = len(series_without_trend) // 24

print("nb_days =", nb_days)

Y = [series_without_trend[i * 24:(i + 1) * 24] for i in range(nb_days)]
percent = nb_pairs_conover_iman(Y, 0.05)
print(percent)

Y = [series_without_trend[i * 24:(i + 1) * 24] for i in range(nb_days)]
groups = nb_groups_kruskal_wallis(Y, 0.05)
print("len(groups) =", len(groups))
lenGroup = []
for g in groups:
    lenGroup.append(round(len(g) / nb_days * 100, 2))
lenGroup = sorted(lenGroup, reverse=True)
print(lenGroup)

for l in groups:
    # print(len(l))
    data = []
    for val in l:
        data += Y[val]
    density_function = st.gaussian_kde(data, bw_method=0.3)
    x = np.linspace(min(data), max(data))
    plt.plot(x, density_function(x))
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
# plt.xlabel('Requests', fontsize=28)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.show()

# half-week

nb_half_weeks = len(series_without_trend) // 84

print("nb_half_weeks =", nb_half_weeks)

Y = [series_without_trend[i * 84:(i + 1) * 84] for i in range(nb_half_weeks)]
percent = nb_pairs_conover_iman(Y, 0.05)
print(percent)

Y = [series_without_trend[i * 84:(i + 1) * 84] for i in range(nb_half_weeks)]
groups = nb_groups_kruskal_wallis(Y, 0.05)
print("len(groups) =", len(groups))
lenGroup = []
for g in groups:
    lenGroup.append(round(len(g) / nb_half_weeks * 100, 2))
lenGroup = sorted(lenGroup, reverse=True)
print(lenGroup)

for l in groups:
    # print(len(l))
    data = []
    for val in l:
        data += Y[val]
    density_function = st.gaussian_kde(data, bw_method=0.3)
    x = np.linspace(min(data), max(data))
    plt.plot(x, density_function(x))
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
# plt.xlabel('Requests', fontsize=28)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.show()

# Week

nb_weeks = len(series_without_trend) // 168

print("nb_weeks =", nb_weeks)

Y = [series_without_trend[i * 168:(i + 1) * 168] for i in range(nb_weeks)]
percent = nb_pairs_conover_iman(Y, 0.05)
print(percent)

Y = [series_without_trend[i * 168:(i + 1) * 168] for i in range(nb_weeks)]
groups = nb_groups_kruskal_wallis(Y, 0.05)
print("len(groups) =", len(groups))
lenGroup = []
for g in groups:
    lenGroup.append(round(len(g) / nb_weeks * 100, 2))
lenGroup = sorted(lenGroup, reverse=True)
print(lenGroup)

for l in groups:
    # print(len(l))
    data = []
    for val in l:
        data += Y[val]
    density_function = st.gaussian_kde(data, bw_method=0.3)
    x = np.linspace(min(data), max(data))
    plt.plot(x, density_function(x))
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
# plt.xlabel('Requests', fontsize=28)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.show()


### Bytes


print("Bytes")

X = []
for i in Wikipedia["Bytes"]:
    X.append(i)

x_abs = [i for i in range(len(X))]
low = lowess(X, x_abs, frac=0.1)
low = [v[1] for v in low]
series_without_trend = [val - tr for val, tr in zip(X, low)]

# Day

nb_days = len(series_without_trend) // 24

print("nb_days =", nb_days)

Y = [series_without_trend[i * 24:(i + 1) * 24] for i in range(nb_days)]
percent = nb_pairs_conover_iman(Y, 0.05)
print(percent)

Y = [series_without_trend[i * 24:(i + 1) * 24] for i in range(nb_days)]
groups = nb_groups_kruskal_wallis(Y, 0.05)
print("len(groups) =", len(groups))
lenGroup = []
for g in groups:
    lenGroup.append(round(len(g) / nb_days * 100, 2))
lenGroup = sorted(lenGroup, reverse=True)
print(lenGroup)

for l in groups:
    # print(len(l))
    data = []
    for val in l:
        data += Y[val]
    density_function = st.gaussian_kde(data, bw_method=0.3)
    x = np.linspace(min(data), max(data))
    plt.plot(x, density_function(x))
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
# plt.xlabel('Bytes', fontsize=28)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.show()

# half-week

nb_half_weeks = len(series_without_trend) // 84

print("nb_half_weeks =", nb_half_weeks)

Y = [series_without_trend[i * 84:(i + 1) * 84] for i in range(nb_half_weeks)]
percent = nb_pairs_conover_iman(Y, 0.05)
print(percent)

Y = [series_without_trend[i * 84:(i + 1) * 84] for i in range(nb_half_weeks)]
groups = nb_groups_kruskal_wallis(Y, 0.05)
print("len(groups) =", len(groups))
lenGroup = []
for g in groups:
    lenGroup.append(round(len(g) / nb_half_weeks * 100, 2))
lenGroup = sorted(lenGroup, reverse=True)
print(lenGroup)

for l in groups:
    # print(len(l))
    data = []
    for val in l:
        data += Y[val]
    density_function = st.gaussian_kde(data, bw_method=0.3)
    x = np.linspace(min(data), max(data))
    plt.plot(x, density_function(x))
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
# plt.xlabel('Bytes', fontsize=28)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.show()

# Week

nb_weeks = len(series_without_trend) // 168

print("nb_weeks =", nb_weeks)

Y = [series_without_trend[i * 168:(i + 1) * 168] for i in range(nb_weeks)]
percent = nb_pairs_conover_iman(Y, 0.05)
print(percent)

Y = [series_without_trend[i * 168:(i + 1) * 168] for i in range(nb_weeks)]
groups = nb_groups_kruskal_wallis(Y, 0.05)
print("len(groups) =", len(groups))
lenGroup = []
for g in groups:
    lenGroup.append(round(len(g) / nb_weeks * 100, 2))
lenGroup = sorted(lenGroup, reverse=True)
print(lenGroup)

for l in groups:
    # print(len(l))
    data = []
    for val in l:
        data += Y[val]
    density_function = st.gaussian_kde(data, bw_method=0.3)
    x = np.linspace(min(data), max(data))
    plt.plot(x, density_function(x))
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
# plt.xlabel('Bytes', fontsize=28)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.show()
