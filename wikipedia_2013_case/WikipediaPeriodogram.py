import json
import matplotlib.pyplot as plt
import pandas as pd
from scipy.signal import periodogram
from statsmodels.tsa.stattools import adfuller, kpss
from statsmodels.tsa.seasonal import MSTL, STL
from statsmodels.nonparametric.smoothers_lowess import lowess

plt.rc('font', size=35)
plt.rcParams['figure.constrained_layout.use'] = True

# Loading data
filename = "Wikipedia2013.json"
with open(filename, 'r') as file:
    Wikipedia = json.load(file)


### Requests time series


# Keeping only request data
Requests = []
for i in Wikipedia["Requests"]:
    Requests.append(i)

print("sum =", sum(Requests))

# Displaying the time series
plt.plot(Requests)
plt.show()

X = [i for i in range(len(Requests))]

# Removing the trend component from the time series
# t0 = time.time()
low = lowess(Requests, X, frac=0.1)
# dt = time.time() - t0
# print("low time:", dt) # approximately 6e-1 s
low = [v[1] for v in low]

# Display of time series and trend component
plt.plot(Requests, label="Time series")
plt.plot(low, label="Trend")
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
plt.xlabel('Time (h)', fontsize=45)
plt.ylabel('Number of requests', fontsize=45)
plt.xticks(fontsize=40)
plt.yticks(fontsize=40)
plt.legend()
plt.show()

# Display STL results with weekly period
# t0 = time.time()
# res = STL(pd.Series(v for v in Requests), period=168).fit()
# dt = time.time() - t0
# print("STL time:", dt) # approximately 1e-1 s
# res.plot()
# plt.show()

# Display MSTL results with daily, half-weekly and weekly periods
# t0 = time.time()
# res = MSTL(Requests, periods=[24, 84, 168]).fit()
# dt = time.time() - t0
# print("MSTL time:", dt) # approximately 2.5 s
# res.plot()
# plt.tight_layout()
# plt.show()

# Res = res.trend.tolist()

Requests = [val - tr for val, tr in zip(Requests, low)]

# Display time series without trend component
plt.plot(Requests, label="Time series")
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
plt.xlabel('Time (h)', fontsize=45)
plt.ylabel('Number of requests', fontsize=45)
plt.xticks(fontsize=40)
plt.yticks(fontsize=40)
plt.legend()
plt.show()

# Periodogram results
# t0 = time.time()
freqencies, spectrum = periodogram(Requests)
# dt = time.time() - t0
# print("Periodogram time:", dt) # approximately 1e-3 s
plt.plot(freqencies, spectrum, color='blue')
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
plt.xlabel('Frequency', fontsize=45)
plt.ylabel('Power spectral density', fontsize=45)
plt.xticks(fontsize=40)
plt.yticks(fontsize=40)
plt.show()

# Top 8 frequencies
values = []
for i, j in zip(list(spectrum), list(freqencies)):
    if j != 0:
        values.append([i, j, 1/j])
    else:
        values.append([i, j, 1e10])
values.sort(reverse=True)
for val in values[0:8]:
    print(val)

print("Number of observations =", len(Requests))

# ADF test
test = adfuller(Requests)

# p-value
print("The p-value ADF:", test[1])

# KPSS test
test = kpss(Requests)

# p-value
print("The p-value KPSS:", test[1])

print("end requests")


### Bytes time series


# Keeping only bytes data
Bytes = []
for i in Wikipedia["Bytes"]:
    Bytes.append(i)

# Displaying the time series
plt.plot(Bytes)
plt.show()

X = [i for i in range(len(Bytes))]

# Removing the trend component from the time series
# t0 = time.time()
low = lowess(Bytes, X, frac=0.1)
low = [v[1] for v in low]
# dt = time.time() - t0

# Display of time series and trend component
plt.plot(Bytes)
plt.plot(low)
plt.show()

Bytes = [val - tr for val, tr in zip(Bytes, low)]

# Periodogram results
freqencies, spectrum = periodogram(Bytes)
plt.plot(freqencies, spectrum, color='blue')
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
plt.xlabel('Frequency', fontsize=45)
plt.ylabel('Power spectral density', fontsize=45)
plt.xticks(fontsize=40)
plt.yticks(fontsize=40)
plt.show()

# Top 8 frequencies
values = []
for i, j in zip(list(spectrum), list(freqencies)):
    if j != 0:
        values.append([i, j, 1/j])
    else:
        values.append([i, j, 1e10])
values.sort(reverse=True)
for val in values[0:8]:
    print(val)

print("Number of observations =", len(Bytes))

# ADF test
test = adfuller(Bytes)

# p-value
print("The p-value ADF:", test[1])

# KPSS test
test = kpss(Bytes)

# p-value
print("The p-value KPSS:", test[1])

print("end bytes")
