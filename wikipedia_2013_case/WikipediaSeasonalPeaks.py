import json
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

plt.rc('font', size=20)
plt.rcParams['figure.constrained_layout.use'] = True


### Functions


def find_peaks(serie): # Computes the percentage of seasonal peaks based on the time of day or the time of day of the week
    peaks_percent = [0] * len(serie[0])
    number_peaks = len(serie)
    for period in serie:
        indexPeak = period.index(max(period))
        peaks_percent[indexPeak] += 1
    for i in range(len(peaks_percent)):
        peaks_percent[i] = peaks_percent[i] / number_peaks
    return peaks_percent


### Requests


nom_fichier = "Wikipedia2013.json"
with open(nom_fichier, 'r') as file:
    Wikipedia = json.load(file)

print("Requests")

X = []
for i in Wikipedia["Requests"]:
    X.append(i)

# Day

nb_days = len(X) // 24

print("nb_days =", nb_days)

Y = [X[i * 24:(i + 1) * 24] for i in range(nb_days)]
x_values = [datetime.datetime(year=2013, month=1, day=7, hour=0, minute=0, second=0) + datetime.timedelta(hours=i) for i in range(24)]
x_v = x_values
peaks = find_peaks(Y)
peaks = peaks[1:] + peaks[:1]
peaks_day_anl_i = peaks
plt.plot(x_values, peaks, color='blue')
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
plt.xlabel('Day (hour)', fontsize=28)
plt.ylabel('Frequency', fontsize=28)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.xticks(rotation = 45)
myFmt = mdates.DateFormatter('%H:%M')
plt.gca().xaxis.set_major_formatter(myFmt)
plt.show()

# Week

nb_weeks = len(X) // 168

print("nb_weeks =", nb_weeks)

Y = [X[i * 168:(i + 1) * 168] for i in range(nb_weeks)]
x_values = [datetime.datetime(year=2013, month=1, day=7, hour=0, minute=0, second=0) + datetime.timedelta(hours=i) for i in range(168)]
x_values = [dt.strftime('%A %H:%M') for dt in x_values]

peaks = find_peaks(Y)
peaks = peaks[145:] + peaks[:145]

plt.plot(x_values, peaks, color='blue')
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
plt.xlabel('Week (hour)', fontsize=28)
plt.ylabel('Frequency', fontsize=28)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.xticks(rotation = 45)
myFmt = mdates.DateFormatter('%A')
plt.gca().xaxis.set_major_formatter(myFmt)
labels_to_print = list(range(0, 168, 12))
plt.xticks(labels_to_print, [x_values[i] for i in labels_to_print])
plt.show()


### Bytes


print("Bytes")

X = []
for i in Wikipedia["Bytes"]:
    X.append(i)

# Day

nb_days = len(X) // 24

print("nb_days =", nb_days)

Y = [X[i * 24:(i + 1) * 24] for i in range(nb_days)]
x_values = [datetime.datetime(year=2013, month=1, day=7, hour=0, minute=0, second=0) + datetime.timedelta(hours=i) for i in range(24)]
x_v = x_values
peaks = find_peaks(Y)
peaks = peaks[1:] + peaks[:1]
peaks_day_anl_i = peaks
plt.plot(x_values, peaks, color='blue')
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
plt.xlabel('Day (hour)', fontsize=28)
plt.ylabel('Frequency', fontsize=28)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.xticks(rotation = 45)
myFmt = mdates.DateFormatter('%H:%M')
plt.gca().xaxis.set_major_formatter(myFmt)
plt.show()

# Week

nb_weeks = len(X) // 168

print("nb_weeks =", nb_weeks)

Y = [X[i * 168:(i + 1) * 168] for i in range(nb_weeks)]
x_values = [datetime.datetime(year=2013, month=1, day=7, hour=0, minute=0, second=0) + datetime.timedelta(hours=i) for i in range(168)]
x_values = [dt.strftime('%A %H:%M') for dt in x_values]

peaks = find_peaks(Y)
peaks = peaks[145:] + peaks[:145]

plt.plot(x_values, peaks, color='blue')
plt.grid(True, linestyle='-', which='major', alpha=0.5, axis='both')
plt.xlabel('Week (hour)', fontsize=28)
plt.ylabel('Frequency', fontsize=28)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.xticks(rotation = 45)
myFmt = mdates.DateFormatter('%A')
plt.gca().xaxis.set_major_formatter(myFmt)
labels_to_print = list(range(0, 168, 12))
plt.xticks(labels_to_print, [x_values[i] for i in labels_to_print])
plt.show()
